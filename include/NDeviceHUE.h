#ifndef NDEVICEHUE_PROTECT
#define NDEVICEHUE_PROTECT

// --------------------
// namespace NDeviceHUE
// --------------------

// namespace NLib
#include "../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NHTTP
#include "../../../NLib/NHTTP/include/NHTTP.h"

// namespace NParser::NJson
#include "../../../NParser/NJson/include/NJson.h"

// namespace NDeviceCommon
#include "../../NDeviceCommon/include/NDeviceCommon.h"

// namespace NDeviceScanner
#include "../../NDeviceScanner/include/NDeviceScanner.h"

// struct NDeviceHUE::NDeviceHUEAuthentication
#include "NDeviceHUE_NDeviceHUEAuthentication.h"

// namespace NDeviceHUE::Service
#include "Service/NDeviceHUE_Service.h"

// namespace NDeviceHUE::Light
#include "Light/NDeviceHUE_Light.h"

// struct NDeviceHUE::NDeviceHUE
#include "NDeviceHUE_NDeviceHUE.h"

#endif // !NDEVICEHUE_PROTECT
