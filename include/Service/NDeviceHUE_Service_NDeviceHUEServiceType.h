#ifndef NDEVICEHUE_SERVICE_NDEVICEHUESERVICETYPE_PROTECT
#define NDEVICEHUE_SERVICE_NDEVICEHUESERVICETYPE_PROTECT

// -----------------------------------------------
// enum NDeviceHUE::Service::NDeviceHUEServiceType
// -----------------------------------------------

typedef enum NDeviceHUEServiceType
{
	NDEVICE_HUE_SERVICE_TYPE_ROOT,

	NDEVICE_HUE_SERVICE_TYPE_AUTHENTICATION,
	NDEVICE_HUE_SERVICE_TYPE_LIGHTS,

	NDEVICE_HUE_SERVICE_TYPES
} NDeviceHUEServiceType;

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceHUE_Service_NDeviceHUEServiceType_GetServiceName( NDeviceHUEServiceType serviceType );

/**
 * Find service type
 *
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type OR NDEVICE_HUE_SERVICE_TYPES
 */
NDeviceHUEServiceType NDeviceHUE_NDeviceHUEServiceType_FindServiceType( const char *element,
	NU32 *cursor );

#ifdef NDEVICEHUE_SERVICE_NDEVICEHUESERVICETYPE_INTERNE
static const char NDeviceHUEServiceTypeRootURI[ NDEVICE_HUE_SERVICE_TYPES ][ 64 ] =
{
	"",

	"authentication",
	"lights"
};
#endif // NDEVICEHUE_SERVICE_NDEVICEHUESERVICETYPE_INTERNE

#endif // !NDEVICEHUE_SERVICE_NDEVICEHUESERVICETYPE_PROTECT
