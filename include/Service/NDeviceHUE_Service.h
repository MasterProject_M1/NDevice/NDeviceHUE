#ifndef NDEVICEHUE_SERVICE_PROTECT
#define NDEVICEHUE_SERVICE_PROTECT

// -----------------------------
// namespace NDeviceHUE::Service
// -----------------------------

// enum NDeviceHUE::Service::NDeviceHUEServiceType
#include "NDeviceHUE_Service_NDeviceHUEServiceType.h"

// enum NDeviceHUE::Service::NDeviceHUEAuthenticationServiceType
#include "NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType.h"

// enum NDeviceHUE::Service::NDeviceHUELightServiceType
#include "NDeviceHUE_Service_NDeviceHUELightListServiceType.h"

// enum NDeviceHUE::Service::NDeviceHUELightServiceType
#include "NDeviceHUE_Service_NDeviceHUELightServiceType.h"

// struct NDeviceHUE::Service::PUT
#include "PUT/NDeviceHUE_Service_PUT.h"

#endif // !NDEVICEHUE_SERVICE_PROTECT
