#ifndef NDEVICEHUE_SERVICE_NDEVICEHUEAUTHENTICATIONSERVICETYPE_PROTECT
#define NDEVICEHUE_SERVICE_NDEVICEHUEAUTHENTICATIONSERVICETYPE_PROTECT

// -------------------------------------------------------------
// enum NDeviceHUE::Service::NDeviceHUEAuthenticationServiceType
// -------------------------------------------------------------

typedef enum NDeviceHUEAuthenticationServiceType
{
	NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_ROOT,

	NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_IP,

	NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_IS_AUTHENTICATED,
	NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_RETRY_COUNT,
	NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_API_KEY,

	NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES
} NDeviceHUEAuthenticationServiceType;

/**
 * Get json key for service
 *
 * @param serviceType
 * 		The service type
 * @param isAddAuthenticationRoot
 * 		Do we add the authentication root?
 *
 * @return the json key
 */
const char *NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_GetJsonKey( NDeviceHUEAuthenticationServiceType serviceType,
	NBOOL isAddAuthenticationRoot );

/**
 * Find service type
 *
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in element
 *
 * @return the service type or NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES
 */
NDeviceHUEAuthenticationServiceType NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_FindService( const char *element,
	NU32 *cursor );

#ifdef NDEVICEHUE_SERVICE_NDEVICEHUEAUTHENTICATIONSERVICETYPE_INTERNE
#define NDEVICE_HUE_AUTHENTICATION_SERVICE_ROOT				"authentication"

static const char NDeviceHUEAuthenticationServiceTypeJson[ NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES ][ 64 ] =
{
	"authentication",

	NDEVICE_HUE_AUTHENTICATION_SERVICE_ROOT".ip",
	NDEVICE_HUE_AUTHENTICATION_SERVICE_ROOT".isAuthenticated",
	NDEVICE_HUE_AUTHENTICATION_SERVICE_ROOT".authenticationRetryCount",
	NDEVICE_HUE_AUTHENTICATION_SERVICE_ROOT".apiKey"
};

static const char NDeviceHUEAuthenticationServiceTypeWithoutRoot[ NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES ][ 64 ] =
{
	"",

	"ip",
	"isAuthenticated",
	"authenticationRetryCount",
	"apiKey"
};
#endif // NDEVICEHUE_SERVICE_NDEVICEHUEAUTHENTICATIONSERVICETYPE_INTERNE

#endif // !NDEVICEHUE_SERVICE_NDEVICEHUEAUTHENTICATIONSERVICETYPE_PROTECT
