#ifndef NDEVICEHUE_SERVICE_NDEVICEHUELIGHTLISTSERVICETYPE_PROTECT
#define NDEVICEHUE_SERVICE_NDEVICEHUELIGHTLISTSERVICETYPE_PROTECT

// --------------------------------------------------------
// enum NDeviceHUE::Service::NDeviceHUELightListServiceType
// --------------------------------------------------------

typedef enum NDeviceHUELightListServiceType
{
	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT,

	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_COUNT,
	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LAST_UPDATE_TIMESTAMP,

	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LIGHT_DETAIL,

	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPES
} NDeviceHUELightListServiceType;

/**
 * Get service key
 *
 * @param serviceType
 * 		The service type
 * @param isAddRootKey
 * 		Do we add the root key?
 *
 * @return the service key
 */
const char *NDeviceHUE_Service_NDeviceHUELightListServiceType_GetServiceKey( NDeviceHUELightListServiceType serviceType,
	NBOOL isAddRootKey );

/**
 * Find service type
 *
 * @param element
 * 		The element to get
 * @param cursor
 * 		The cursor into element
 * @param lightList
 * 		The light list
 * @param lightIndexOutput
 * 		The light index found
 *
 * @return the service type
 */
NDeviceHUELightListServiceType NDeviceHUE_Service_NDeviceHUELightListServiceType_FindService( const char *element,
	NU32 *cursor,
	const void *lightList,
	NU32 *lightIndexOutput );

#ifdef NDEVICEHUE_SERVICE_NDEVICEHUELIGHTLISTSERVICETYPE_INTERNE
#define NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT_KEY				"lights"

static const char NDeviceHUELightListServiceTypeJson[ NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPES ][ 64 ] =
{
	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT_KEY,

	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT_KEY".count",
	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT_KEY".lastUpdate",

	NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT_KEY""
};

static const char NDeviceHUELightListServiceTypeWithoutRoot[ NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPES ][ 64 ] =
{
	"",

	"count",
	"lastUpdate",

	""
};
#endif // NDEVICEHUE_SERVICE_NDEVICEHUELIGHTLISTSERVICETYPE_INTERNE

#endif // !NDEVICEHUE_SERVICE_NDEVICEHUELIGHTLISTSERVICETYPE_PROTECT
