#ifndef NDEVICEHUE_SERVICE_PUT_PROTECT
#define NDEVICEHUE_SERVICE_PUT_PROTECT

// ----------------------------------
// namespace NDeviceHUE::Service::PUT
// ----------------------------------

// struct NDeviceHUE::Service::PUT::NHueLightModification
#include "NDeviceHUE_Service_PUT_NHUELightModification.h"

#endif // !NDEVICEHUE_SERVICE_PUT_PROTECT
