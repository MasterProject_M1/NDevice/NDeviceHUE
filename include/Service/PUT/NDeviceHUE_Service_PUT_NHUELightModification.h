#ifndef NDEVICEHUE_SERVICE_PUT_NHUELIGHTMODIFICATION_PROTECT
#define NDEVICEHUE_SERVICE_PUT_NHUELIGHTMODIFICATION_PROTECT

// ------------------------------------------------------
// struct NDeviceHUE::Service::PUT::NHUELightModification
// ------------------------------------------------------

typedef struct NHUELightModification
{
	// Light
	const void *m_light;

	// Properties to be set
	NParserOutputList *m_property;
} NHUELightModification;

/**
 * Build light modification list
 *
 * @param light
 * 		The light
 *
 * @return the built instance
 */
__ALLOC NHUELightModification *NDeviceHUE_Service_PUT_NHUELightModification_Build( const void *light );

/**
 * Destroy modification list
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Service_PUT_NHUELightModification_Destroy( NHUELightModification** );

/**
 * Set light
 *
 * @param this
 * 		This instance
 * @param light
 * 		The light
 */
void NDeviceHUE_Service_PUT_NHUELightModification_SetLight( NHUELightModification*,
	const void *light );

/**
 * Add a property with string value
 *
 * @param this
 * 		This instance
 * @param hueLightProperty
 * 		The hue light property property
 * @param value
 * 		The string value
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyString( NHUELightModification*,
	NU32 hueLightProperty,
	const char *value );

/**
 * Add a property with integer value
 *
 * @param this
 * 		This instance
 * @param hueLightProperty
 * 		The hue light property property
 * @param value
 * 		The integer value
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyInteger( NHUELightModification*,
	NU32 hueLightProperty,
	NU32 value );

/**
 * Add a property with double value
 *
 * @param this
 * 		This instance
 * @param hueLightProperty
 * 		The hue light property property
 * @param value
 * 		The double value
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyDouble( NHUELightModification*,
	NU32 hueLightProperty,
	double value );

/**
 * Add a property with boolean value
 *
 * @param this
 * 		This instance
 * @param hueLightProperty
 * 		The hue light property property
 * @param value
 * 		The boolean value
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyBoolean( NHUELightModification*,
	NU32 hueLightProperty,
	NBOOL value );

/**
 * Add property
 *
 * @param this
 * 		This instance
 * @param parserOutput
 * 		A parser entry
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddProperty( NHUELightModification*,
	NDeviceHUELightServiceType serviceType,
	const NParserOutput *parserOutput );

/**
 * Send modification to light
 *
 * @param this
 * 		This instance
 * @param isStateModification
 * 		Is it a state change or a light property change?
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_SendModification( NHUELightModification*,
	NBOOL isStateModification );

/**
 * Is modification present?
 *
 * @param this
 * 		This instance
 *
 * @return if there are modifications to be applied
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_IsModification( const NHUELightModification* );

#endif // !NDEVICEHUE_SERVICE_PUT_NHUELIGHTMODIFICATION_PROTECT
