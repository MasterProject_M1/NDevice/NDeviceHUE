#ifndef NDEVICEHUE_NDEVICEHUE_PROTECT
#define NDEVICEHUE_NDEVICEHUE_PROTECT

// -----------------------------
// struct NDeviceHUE::NDeviceHUE
// -----------------------------

typedef struct NDeviceHUE
{
	// IP
	char *m_ip;

	// Authentication
	NDeviceHUEAuthentication *m_authentication;

	// Lights list
	NHUELightList *m_light;

} NDeviceHUE;

/**
 * Build a hue device
 *
 * @param name
 * 		The device name
 * @param ip
 * 		The device IP
 * @param maximumAuthenticationRetryCount
 * 		The maximum authentication attempts (0 = infinite)
 * @param autoUpdateDelay
 * 		The auto light state update delay
 *
 * @return the device instance
 */
__ALLOC NDeviceHUE *NDeviceHUE_NDeviceHUE_Build( const char *name,
	const char *ip,
	NU32 maximumAuthenticationRetryCount,
	NU32 autoUpdateDelay );

/**
 * Destroy a hue device
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_NDeviceHUE_Destroy( NDeviceHUE** );

/**
 * Get hue device name
 *
 * @param this
 * 		This instance
 *
 * @return the name
 */
const char *NDeviceHUE_NDeviceHUE_GetName( const NDeviceHUE* );

/**
 * Is ready? (Authenticated?)
 *
 * @param this
 * 		This instance
 *
 * @return if the device is ready (authenticated)
 */
NBOOL NDeviceHUE_NDeviceHUE_IsReady( const NDeviceHUE* );

/**
 * Get hue device authentication ID
 *
 * @param this
 * 		This instance
 *
 * @return the id or NULL if not got yet
 */
const char *NDeviceHUE_NDeviceHUE_GetAuthenticationID( const NDeviceHUE* );

/**
 * Get hue device IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NDeviceHUE_NDeviceHUE_GetIP( const NDeviceHUE* );

/**
 * Get light list
 *
 * @param this
 * 		This instance
 *
 * @return the light list
 */
NHUELightList *NDeviceHUE_NDeviceHUE_GetLightList( const NDeviceHUE* );

/**
 * Update light list
 *
 * @param this
 * 		This instance
 *
 * @return if operation started successfully
 */
NBOOL NDeviceHUE_NDeviceHUE_UpdateLightList( NDeviceHUE* );

/**
 * Build parser output list describing the current device state
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_NDeviceHUE_BuildParserOutputList( const NDeviceHUE*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process GET REST request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in element
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_NDeviceHUE_ProcessRESTGETRequest( const NDeviceHUE*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *element,
	NU32 *cursor );

/**
 * Process REST PUT request
 *
 * @param this
 * 		This instance
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param clientDataJson
 * 		The client parsed data
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_NDeviceHUE_ProcessRESTPUTRequest( const NDeviceHUE*,
	const char *element,
	NU32 *cursor,
	const NParserOutputList *clientDataJson );

#endif // !NDEVICEHUE_NDEVICEHUE_PROTECT
