#ifndef NDEVICEHUE_LIGHT_NHUELIGHTALERT_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHTALERT_PROTECT

// --------------------------------------
// enum NDeviceHUE::Light::NHUELightAlert
// --------------------------------------

typedef enum NHUELightAlert
{
	NHUE_LIGHT_ALERT_NONE,

	NHUE_LIGHT_ALERT_SELECT,
	NHUE_LIGHT_ALERT_LONG_SELECT,

	NHUE_LIGHT_ALERTS
} NHUELightAlert;

/**
 * Parser light alert
 *
 * @param alert
 * 		The alert
 *
 * @return the light alert
 */
NHUELightAlert NDeviceHUE_Light_NHUELightAlert_Parse( const char *alert );

/**
 * Get the light alert name
 *
 * @param alert
 * 		The alert
 *
 * @return the light alert name
 */
const char *NDeviceHUE_Light_NHUELightAlert_GetName( NHUELightAlert alert );

#ifdef NDEVICEHUE_LIGHT_NHUELIGHTALERT_INTERNE
static const char NHUELightAlertName[ NHUE_LIGHT_ALERTS ][ 32 ] =
{
	"none",

	"select",
	"lselect"
};
#endif // NDEVICEHUE_LIGHT_NHUELIGHTALERT_INTERNE

#endif // !NDEVICEHUE_LIGHT_NHUELIGHTALERT_PROTECT
