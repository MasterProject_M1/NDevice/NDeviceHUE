#ifndef NDEVICEHUE_LIGHT_NHUELIGHTSTATE_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHTSTATE_PROTECT

// ----------------------------------------
// struct NDeviceHUE::Light::NHUELightState
// ----------------------------------------

typedef struct NHUELightState
{
	// Is on?
	NBOOL m_isOn;

	// Is reachable
	NBOOL m_isReachable;

	// Brightness
	NU8 m_brightness;

	// Saturation
	NU8 m_saturation;

	// HUE
	NU16 m_hue;

	// XY
	NDoublePoint m_xy;

	// Color temperature
	NU32 m_colorTemperature;

	// Effect
	NHUELightEffect m_effect;

	// Alert
	NHUELightAlert m_alert;

	// Color mode
	NHUELightColorMode m_colorMode;
} NHUELightState;

/**
 * Build light state
 *
 * @param lightDetail
 * 		The lights details
 * @param id
 * 		The light id
 *
 * @return the built instance
 */
__ALLOC NHUELightState *NDeviceHUE_Light_NHUELightState_Build( const NParserOutputList *lightDetail,
	NU32 id );

/**
 * Destroy instance
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Light_NHUELightState_Destroy( NHUELightState** );

/**
 * Is light on?
 *
 * @param this
 * 		This instance
 *
 * @return if the light is on
 */
NBOOL NDeviceHUE_Light_NHUELightState_IsOn( const NHUELightState* );

/**
 * Is reachable?
 *
 * @param this
 * 		This instance
 *
 * @return if the light is reachable
 */
NBOOL NDeviceHUE_Light_NHUELightState_IsReachable( const NHUELightState* );

/**
 * Get the brightness
 *
 * @param this
 * 		This instance
 *
 * @return the brightness
 */
NU8 NDeviceHUE_Light_NHUELightState_GetBrightness( const NHUELightState* );

/**
 * Get the saturation
 *
 * @param this
 * 		This instance
 *
 * @return the saturation
 */
NU8 NDeviceHUE_Light_NHUELightState_GetSaturation( const NHUELightState* );

/**
 * Get the hue
 *
 * @param this
 * 		This instance
 *
 * @return the hue
 */
NU16 NDeviceHUE_Light_NHUELightState_GetHue( const NHUELightState* );

/**
 * Get the xy color
 *
 * @param this
 * 		This instance
 *
 * @return the color at xy format
 */
NDoublePoint NDeviceHUE_Light_NHUELightState_GetXYColor( const NHUELightState* );

/**
 * Get the color temperature
 *
 * @param this
 * 		This instance
 *
 * @return the color temperature
 */
NU32 NDeviceHUE_Light_NHUELightState_GetColorTemperature( const NHUELightState* );

/**
 * Get light effect
 *
 * @param this
 * 		This instance
 *
 * @return the light effect
 */
NHUELightEffect NDeviceHUE_Light_NHUELightState_GetEffect( const NHUELightState* );

/**
 * Get alert
 *
 * @param this
 * 		This instance
 *
 * @return the light alert
 */
NHUELightAlert NDeviceHUE_Light_NHUELightState_GetAlert( const NHUELightState* );

/**
 * Get color mode
 *
 * @param this
 * 		This instance
 *
 * @return the light color mode
 */
NHUELightColorMode NDeviceHUE_Light_NHUELightState_GetColorMode( const NHUELightState* );

/**
 * Display current light state
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Light_NHUELightState_Display( const NHUELightState* );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELightState_BuildParserOutputListByService( const NHUELightState*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NDeviceHUELightServiceType serviceType );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELightState_BuildParserOutputList( const NHUELightState*,
	__OUTPUT NParserOutputList *parserOutputList );

#endif // !NDEVICEHUE_LIGHT_NHUELIGHTSTATE_PROTECT

