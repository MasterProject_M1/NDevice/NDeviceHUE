#ifndef NDEVICEHUE_LIGHT_NHUELIGHTCOLOR_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHTCOLOR_PROTECT

// --------------------------------------
// enum NDeviceHUE::Light::NHUELightColor
// --------------------------------------

typedef enum NHUELightColor
{
	NHUE_LIGHT_COLOR_RED,
	NHUE_LIGHT_COLOR_GREEN,
	NHUE_LIGHT_COLOR_BLUE,
	NHUE_LIGHT_COLOR_WHITE,

	NHUE_LIGHT_COLORS
} NHUELightColor;

/**
 * Get values for color name
 *
 * @param colorName
 * 		The color name
 *
 * @return the RGB array values
 */
const NU8 *NDeviceHUE_Light_NHUELightColor_FindValueFromName( const char *colorName );

#ifdef NDEVICEHUE_LIGHT_NHUELIGHTCOLOR_INTERNE
__PRIVATE NU8 NHUELightColorValue[ NHUE_LIGHT_COLORS ][ 3 ] =
{
	{ 255, 0, 0 },
	{ 0, 255, 0 },
	{ 0, 0, 255 },
	{ 255, 255, 255 }
};

__PRIVATE const char NHUELightColorName[ NHUE_LIGHT_COLORS ][ 32 ] =
{
	"Rouge",
	"Vert",
	"Bleu",
	"Blanc"
};
#endif // NDEVICEHUE_LIGHT_NHUELIGHTCOLOR_INTERNE

#endif // !NDEVICEHUE_LIGHT_NHUELIGHTCOLOR_PROTECT
