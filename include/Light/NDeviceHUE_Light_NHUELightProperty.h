#ifndef NDEVICEHUE_LIGHT_NHUELIGHTPROPERTY_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHTPROPERTY_PROTECT

// -----------------------------------------
// enum NDeviceHUE::Light::NHUELightProperty
// -----------------------------------------

/**
 * "alert":"select", "on":false ==> Quick flash
 */
typedef enum NHUELightProperty
{
	NHUE_LIGHT_PROPERTY_STATE_IS_ON,
	NHUE_LIGHT_PROPERTY_STATE_BRIGHTNESS,
	NHUE_LIGHT_PROPERTY_STATE_COLOR_HUE, // 0 to 65535, "Both 0 and 65535 are red, 25500 is green and 46920 is blue."
	NHUE_LIGHT_PROPERTY_STATE_COLOR_SATURATION, // 0 -> White, 255 -> Colored
	NHUE_LIGHT_PROPERTY_STATE_EFFECT, // {"effect":"colorloop"} -> Random color
	NHUE_LIGHT_PROPERTY_STATE_COLOR_XY, // Give 2 values, first=x, second=y
	NHUE_LIGHT_PROPERTY_STATE_COLOR_TEMPERATURE,
	NHUE_LIGHT_PROPERTY_STATE_ALERT, // {"alert":"select"} -> Blink (~500ms)
	NHUE_LIGHT_PROPERTY_STATE_COLOR_MODE,
	NHUE_LIGHT_PROPERTY_STATE_IS_REACHABLE,

	NHUE_LIGHT_PROPERTY_NAME,
	NHUE_LIGHT_PROPERTY_TYPE,
	NHUE_LIGHT_PROPERTY_MODEL_ID,
	NHUE_LIGHT_PROPERTY_SOFTWARE_VERSION,
	NHUE_LIGHT_PROPERTY_MANUFACTURER_NAME,
	NHUE_LIGHT_PROPERTY_UNIQUE_ID,

	NHUE_LIGHT_PROPERTIES
} NHUELightProperty;

/**
 * Get the property name
 *
 * @param property
 * 		The property
 *
 * @return the parser compatible name
 */
const char *NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUELightProperty property );

/**
 * Get short state property
 *
 * @param property
 * 		The state property
 *
 * @return the short json property
 */
const char *NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUELightProperty property );

#ifdef NDEVICEHUE_LIGHT_NHUELIGHTPROPERTY_INTERNE
static const char NHUEStateLightPropertyJson[ NHUE_LIGHT_PROPERTIES ][ 32 ] =
{
	"on",
	"bri",
	"hue",
	"sat",
	"effect",
	"xy",
	"ct",
	"alert",
	"colormode",
	"reachable",

	"",
	"",
	"",
	"",
	"",
	""
};

static const char NHUELightPropertyJson[ NHUE_LIGHT_PROPERTIES ][ 64 ] =
{
	"state.on",
	"state.bri",
	"state.hue",
	"state.sat",
	"state.effect",
	"state.xy",
	"state.ct",
	"state.alert",
	"state.colormode",
	"state.reachable",

	"name",
	"type",
	"modelid",
	"swversion",
	"manufacturername",
	"uniqueid"
};
#endif // NDEVICEHUE_LIGHT_NHUELIGHTPROPERTY_INTERNE

#endif // !NDEVICEHUE_LIGHT_NHUELIGHTPROPERTY_PROTECT
