#ifndef NDEVICEHUE_LIGHT_NHUELIGHTTYPE_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHTTYPE_PROTECT

// -------------------------------------
// enum NDeviceHUE::Light::NHUELightType
// -------------------------------------

typedef enum NHUELightType
{
	NHUE_LIGHT_TYPE_LIGHT_BULB,
	NHUE_LIGHT_TYPE_LIGHT_STRIP,
	NHUE_LIGHT_TYPE_LIGHT_GLOBE,

	NHUE_LIGHT_TYPES
} NHUELightType;

/**
 * Find light type
 *
 * @param technicalID
 * 		The light technical id
 *
 * @return the light type or NHUE_LIGHT_TYPES if not found
 */
NHUELightType NDeviceHUE_Light_NHUELightType_FindLightType( const char *technicalID );

/**
 * Get light name
 *
 * @param lightType
 * 		The light type
 *
 * @return the light name
 */
const char *NDeviceHUE_Light_NHUELightType_GetLightName( NHUELightType lightType );

#ifdef NDEVICEHUE_LIGHT_NHUELIGHTTYPE_INTERNE
static const char NHUELightTypeName[ NHUE_LIGHT_TYPES ][ 32 ] =
{
	"Bulb",
	"Strip",
	"Globe"
};

static const char NHUELightTypeTechnicalID[ NHUE_LIGHT_TYPES ][ 32 ] =
{
	"LCT015",
	"LST002",
	"LLC020"
};
#endif // NDEVICEHUE_LIGHT_NHUELIGHTTYPE_INTERNE

#endif // !NDEVICEHUE_LIGHT_NHUELIGHTTYPE_PROTECT

