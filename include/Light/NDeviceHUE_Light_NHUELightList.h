#ifndef NDEVICEHUE_LIGHT_NHUELIGHTLIST_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHTLIST_PROTECT

// ---------------------------------------
// struct NDeviceHUE::Light::NHUELightList
// ---------------------------------------

/**
 * As the light update is asynchronous, the mutex on m_light
 * has to be lock to use its content.
 */

typedef struct NHUELightList
{
	// Authentication
	const NDeviceHUEAuthentication *m_authentication;

	// Light list
	NListe *m_light;

	// Light obtention thread
	NThread *m_lightObtentionThread;

	// Is waiting for light list?
	NBOOL m_isWaitingForLightList;

	// Last lights state update
	NU64 m_lastLightUpdateTimestamp;

	// Auto update delay (ms)
	NU32 m_autoUpdateDelay;

	// Light update thread
	NBOOL m_isAutoUpdateThreadRunning;
	NThread *m_lightUpdateThread;
} NHUELightList;

/**
 * Build the lights list
 *
 * @param authentication
 * 		The authentication informations
 * @param autoUpdateDelay
 * 		The auto light state update delay (ms) [Not updating if 0]
 *
 * @return the light list instance
 */
__ALLOC NHUELightList *NDeviceHUE_Light_NHUELightList_Build( const NDeviceHUEAuthentication *authentication,
	NU32 autoUpdateDelay );

/**
 * Destroy light list
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Light_NHUELightList_Destroy( NHUELightList** );

/**
 * Update light list
 *
 * @param this
 * 		This instance
 *
 * @return if update successfully started
 */
NBOOL NDeviceHUE_Light_NHUELightList_UpdateLightList( NHUELightList* );

/**
 * Activate protection on list
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NDeviceHUE_Light_NHUELightList_ActivateProtection( NHUELightList* );

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NDeviceHUE_Light_NHUELightList_DeactivateProtection( NHUELightList* );

/**
 * Find a light by id
 *
 * @param this
 * 		This instance
 * @param id
 * 		The light id
 *
 * @return the light
 */
__MUSTBEPROTECTED NHUELight *NDeviceHUE_Light_NHUELightList_FindLightByID( NHUELightList*,
	NU32 id );

/**
 * Find a light by name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The light name
 *
 * @return the light
 */
__MUSTBEPROTECTED NHUELight *NDeviceHUE_Light_NHUELightList_FindLightByName( NHUELightList*,
	const char *name );

/**
 * Get light count
 *
 * @param this
 * 		This instance
 *
 * @return the light count
 */
__MUSTBEPROTECTED NU32 NDeviceHUE_Light_NHUELightList_GetLightCount( const NHUELightList* );

/**
 * Get light by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The light index
 *
 * @return the light
 */
__MUSTBEPROTECTED const NHUELight *NDeviceHUE_Light_NHUELightList_GetLightByIndex( const NHUELightList*,
	NU32 index );

/**
 * Activate a light
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ActivateLight( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId );

/**
 * Deactivate a light
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_DeactivateLight( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId );

/**
 * Short blink a light (will shutdown the light after blink)
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ShortBlinkLight( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId );

/**
 * Blink a light
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_BlinkLight( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId );

/**
 * Long blink a light (for 15 seconds, with a period of 1 second)
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_LongBlinkLight( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId );

/**
 * Activate color loop
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ActivateColorLoop( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId );

/**
 * Deactivate color loop
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_DeactivateColorLoop( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId );

/**
 * Change light intensity
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param brightness
 * 		The light intensity (1-254)
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeIntensity( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU8 brightness );

/**
 * Change color and intensity
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param hue
 * 		The hue value (0 to 65535)
 * @param saturation
 * 		The saturation value (0 to 255)
 * @param brightness
 * 		The brightness (0 to 255)
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColorAndIntensity( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU16 hue,
	NU8 saturation,
	NU8 brightness );

/**
 * Change light color with hue/saturation combination
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param hue
 * 		The hue value (0 to 65535)
 * @param saturation
 * 		The saturation value
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU16 hue,
	NU8 saturation );

/**
 * Change light color with xy combination
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param x
 * 		The x color coordinate
 * @param y
 * 		The y color coordinate
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor2( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	double x,
	double y );

/**
 * Change light color with hue/saturation combination
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param colorTemperature
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor3( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU16 colorTemperature );

/**
 * Change color with rgb value
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param r
 * 		The red value
 * @param g
 * 		The green value
 * @param b
 * 		The blue value
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor4( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU8 r,
	NU8 g,
	NU8 b );

/**
 * Change color with rgb value
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param color
 * 		The rgb color
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor5( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NCouleur color );

/**
 * Change color with color name
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param colorName
 * 		The color name
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor6( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	const char *colorName );

/**
 * Set the light name
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param name
 * 		The new light name
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeName( NHUELightList*,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	const char *name );

/**
 * Build parser output state
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param isAddRootKey
 * 		Do we add root key?
 *
 * @return if operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceHUE_Light_NHUELightList_BuildParserOutputList( const NHUELightList*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NBOOL isAddRootKey );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param element
 * 		The element to GET
 * @param cursor
 * 		Cursor in element
 *
 * @return if operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceHUE_Light_NHUELightList_ProcessRESTGETRequest( const NHUELightList*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *element,
	NU32 *cursor );

/**
 * Process REST PUT request
 *
 * @param this
 * 		This instance
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param clientDataJson
 * 		The client parsed data
 * @param currentLevel
 * 		The current level
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELightList_ProcessRESTPUTRequest( const NHUELightList*,
	const char *element,
	NU32 *cursor,
	const NParserOutputList *clientDataJson );

#endif // !NDEVICEHUE_LIGHT_NHUELIGHTLIST_PROTECT
