#ifndef NDEVICEHUE_LIGHT_NHUELIGHT_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHT_PROTECT

// -----------------------------------
// struct NDeviceHUE::Light::NHUELight
// -----------------------------------

typedef struct NHUELight
{
	// The light id
	NU32 m_id;

	// The light name
	char *m_name;

	// The API link for this light
	char *m_apiLightStateRequestLink;

	// Type
	NHUELightType m_type;

	// The client
	NClientHTTP *m_client;

	// Authentication
	const NDeviceHUEAuthentication *m_authentication;

	// State
	NHUELightState *m_state;
} NHUELight;

/**
 * Build a light
 *
 * @param lightInformation
 * 		The light information details
 * @param lightType
 * 		The light type
 * @param authentication
 * 		The hue device authentication
 * @param allLightInformation
 * 		The json parsed data
 *
 * @return the light instance
 */
__ALLOC NHUELight *NDeviceHUE_Light_NHUELight_Build( const char *lightInformation,
	const char *lightType,
	const NDeviceHUEAuthentication *authentication,
	const NParserOutputList *allLightInformation );

/**
 * Destroy a light
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Light_NHUELight_Destroy( NHUELight** );

/**
 * Send a request
 *
 * @param this
 * 		This instance
 * @param httpRequestType
 * 		The http request type
 * @param apiElement
 * 		The api element to request
 * @param jsonRequest
 * 		The request to send
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_SendRequest( const NHUELight*,
	NTypeRequeteHTTP httpRequestType,
	const char *apiElement,
	const char *jsonRequest );

/**
 * Send a request
 *
 * @param this
 * 		This instance
 * @param httpRequestType
 * 		The http request type
 * @param apiElement
 * 		The api element to request
 * @param jsonRequest
 * 		The request to send
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_SendRequest2( const NHUELight*,
	NTypeRequeteHTTP httpRequestType,
	const char *jsonRequest );


/**
 * Send root request
 *
 * @param this
 * 		This instance
 * @param httpRequestType
 * 		The http request type
 * @param jsonRequest
 * 		The json request to be done
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_SendRootRequest( const NHUELight*,
	NTypeRequeteHTTP httpRequestType,
	const char *jsonRequest );

/**
 * Get light id
 *
 * @param this
 * 		This instance
 *
 * @return the light id
 */
NU32 NDeviceHUE_Light_NHUELight_GetID( const NHUELight* );

/**
 * Get light type
 *
 * @param this
 * 		This instance
 *
 * @return the type
 */
NHUELightType NDeviceHUE_Light_NHUELight_GetType( const NHUELight* );

/**
 * Get light name
 *
 * @param this
 * 		This instance
 *
 * @return the light name
 */
const char *NDeviceHUE_Light_NHUELight_GetName( const NHUELight* );

/**
 * Get state
 *
 * @param this
 * 		This instance
 *
 * @return the last known light state
 */
const NHUELightState *NDeviceHUE_Light_NHUELight_GetState( const NHUELight* );

/**
 * Activate a light
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ActivateLight( NHUELight* );

/**
 * Deactivate a light
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_DeactivateLight( NHUELight* );

/**
 * Short blink a light (will shutdown the light after blink)
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ShortBlinkLight( NHUELight* );

/**
 * Blink a light
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_BlinkLight( NHUELight* );

/**
 * Long blink a light (for 15 seconds, with a period of 1 second)
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_LongBlinkLight( NHUELight* );

/**
 * Activate color loop
 *
 * @param this
 * 		This instance
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ActivateColorLoop( NHUELight* );

/**
 * Deactivate color loop
 *
 * @param this
 * 		This instance
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_DeactivateColorLoop( NHUELight* );

/**
 * Change light intensity
 *
 * @param this
 * 		This instance
 * @param brightness
 * 		The light intensity (1-254)
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeIntensity( NHUELight*,
	NU8 brightness );

/**
 * Change color and intensity
 *
 * @param this
 * 		This instance
 * @param hue
 * 		The hue value (0 to 65535)
 * @param saturation
 * 		The saturation value (0 to 255)
 * @param brightness
 * 		The brightness (0 to 255)
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColorAndIntensity( NHUELight*,
	NU16 hue,
	NU8 saturation,
	NU8 brightness );

/**
 * Change light color with hue/saturation combination
 *
 * @param this
 * 		This instance
 * @param hue
 * 		The hue value (0 to 65535)
 * @param saturation
 * 		The saturation value
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor( NHUELight*,
	NU16 hue,
	NU8 saturation );

/**
 * Change light color with xy combination
 *
 * @param this
 * 		This instance
 * @param x
 * 		The x color coordinate
 * @param y
 * 		The y color coordinate
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor2( NHUELight*,
	double x,
	double y );

/**
 * Change light color with color temperature
 *
 * @param this
 * 		This instance
 * @param colorTemperature
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor3( NHUELight*,
	NU16 colorTemperature );

/**
 * Change color with rgb value
 *
 * @param this
 * 		This instance
 * @param r
 * 		The red value
 * @param g
 * 		The green value
 * @param b
 * 		The blue value
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor4( NHUELight*,
	NU8 r,
	NU8 g,
	NU8 b );

/**
 * Change color with rgb value
 *
 * @param this
 * 		This instance
 * @param color
 * 		The rgb color
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor5( NHUELight*,
	NCouleur color );

/**
 * Set the light name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The new light name
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeName( NHUELight*,
	const char *name );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_BuildParserOutputList( const NHUELight*,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor into element
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_ProcessRESTGetRequest( const NHUELight*,
	NParserOutputList *parserOutputList,
	const char *element,
	NU32 *cursor );

/**
 * Process REST PUT request
 *
 * @param this
 * 		This instance
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor
 * @param clientDataJson
 * 		The client parsed data
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_ProcessRESTPUTRequest( NHUELight*,
	const char *element,
	NU32 cursor,
	const NParserOutputList *clientDataJson );

#endif // !NDEVICEHUE_LIGHT_NHUELIGHT_PROTECT
