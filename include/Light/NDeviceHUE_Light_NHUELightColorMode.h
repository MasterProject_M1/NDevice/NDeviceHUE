#ifndef NDEVICEHUE_LIGHT_NHUELIGHTCOLORMODE_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHTCOLORMODE_PROTECT

// ------------------------------------------
// enum NDeviceHUE::Light::NHUELightColorMode
// ------------------------------------------

typedef enum NHUELightColorMode
{
	NHUE_LIGHT_COLOR_MODE_HUE_SATURATION,
	NHUE_LIGHT_COLOR_MODE_XY,
	NHUE_LIGHT_COLOR_MODE_COLOR_TEMPERATURE,

	NHUE_LIGHT_COLOR_MODES
} NHUELightColorMode;

/**
 * Parser light color mode
 *
 * @param mode
 * 		The light color mode
 *
 * @return the light color mode
 */
NHUELightColorMode NDeviceHUE_Light_NHUELightColorMode_Parse( const char *mode );

/**
 * Get light color mode name
 *
 * @param mode
 * 		The light color mode
 *
 * @return the light color mode name
 */
const char *NDeviceHUE_Light_NHUELightColorMode_GetName( NHUELightColorMode mode );

#ifdef NDEVICEHUE_LIGHT_NHUELIGHTCOLORMODE_INTERNE
static const char NHUELightColorModeName[ NHUE_LIGHT_COLOR_MODES ][ 32 ] =
{
	"hs",
	"xy",
	"ct"
};
#endif // NDEVICEHUE_LIGHT_NHUELIGHTCOLORMODE_INTERNE

#endif // !NDEVICEHUE_LIGHT_NHUELIGHTCOLORMODE_PROTECT

