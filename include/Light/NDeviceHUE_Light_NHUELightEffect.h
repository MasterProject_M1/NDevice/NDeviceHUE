#ifndef NDEVICEHUE_LIGHT_NHUELIGHTEFFECT_PROTECT
#define NDEVICEHUE_LIGHT_NHUELIGHTEFFECT_PROTECT

// ---------------------------------------
// enum NDeviceHUE::Light::NHUELightEffect
// ---------------------------------------

typedef enum NHUELightEffect
{
	NHUE_LIGHT_EFFECT_NONE,

	NHUE_LIGHT_EFFECT_COLOR_LOOP,

	NHUE_LIGHT_EFFECTS
} NHUELightEffect;

/**
 * Parser light effect
 *
 * @param effect
 * 		The light effect
 *
 * @return the light effect
 */
NHUELightEffect NDeviceHUE_Light_NHUELightEffect_Parse( const char *effect );

/**
 * Get light effect name
 *
 * @param effect
 * 		The light effect
 *
 * @return the light effect name
 */
const char *NDeviceHUE_Light_NHUELightEffect_GetName( NHUELightEffect effect );

#ifdef NDEVICEHUE_LIGHT_NHUELIGHTEFFECT_INTERNE
static const char NHUELightEffectName[ NHUE_LIGHT_EFFECTS ][ 32 ] =
{
	"none",

	"colorloop"
};
#endif // NDEVICEHUE_LIGHT_NHUELIGHTEFFECT_INTERNE

#endif // !NDEVICEHUE_LIGHT_NHUELIGHTEFFECT_PROTECT

