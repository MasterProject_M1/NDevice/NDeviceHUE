#ifndef NDEVICEHUE_LIGHT_PROTECT
#define NDEVICEHUE_LIGHT_PROTECT

// ---------------------------
// namespace NDeviceHUE::Light
// ---------------------------

// enum NDeviceHUE::Light::NHUELightType
#include "NDeviceHUE_Light_NHUELightType.h"

// enum NDeviceHUE::Light::NHUELightEffect
#include "NDeviceHUE_Light_NHUELightEffect.h"

// enum NDeviceHUE::Light::NHUELightAlert
#include "NDeviceHUE_Light_NHUELightAlert.h"

// enum NDeviceHUE::Light::NHUELightColor
#include "NDeviceHUE_Light_NHUELightColor.h"

// enum NDeviceHUE::Light::NHUELightColorMode
#include "NDeviceHUE_Light_NHUELightColorMode.h"

// enum NDeviceHUE::Light::NHUELightProperty
#include "NDeviceHUE_Light_NHUELightProperty.h"

// struct NDeviceHUE::Light::NHUELightState
#include "NDeviceHUE_Light_NHUELightState.h"

// struct NDeviceHUE::Light::NHUELight
#include "NDeviceHUE_Light_NHUELight.h"

// struct NDeviceHUE::Light::NHueLightList
#include "NDeviceHUE_Light_NHUELightList.h"

/**
 * Convert rgb to hue/saturation/brightness
 *
 * @param color
 * 		The rgb color
 * @param hue
 * 		The output hue
 * @param saturation
 * 		The output saturation
 * @param brightness
 * 		The output brightness
 */
void NDeviceHUE_Light_ConvertRGBToHUESaturationBrightness( NU8 r,
	NU8 g,
	NU8 b,
	__OUTPUT NU16 *hue,
	__OUTPUT NU8 *saturation,
	__OUTPUT NU8 *brightness );

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 * (https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion)
 *
 * @param   {number}  h       The hue
 * @param   {number}  s       The saturation
 * @param   {number}  l       The lightness
 * @return  {Array}           The RGB representation
 */
void NDeviceHUE_Light_ConvertHUESaturationBrightnessToRGB( NU16 hue,
	NU8 saturation,
	NU8 brightness,
	__OUTPUT NU8 *r,
	__OUTPUT NU8 *g,
	__OUTPUT NU8 *b );

/**
 * Process RGB property to add correct attributes
 *
 * @param clientJsonData
 * 		The client data
 * @param lightModification
 * 		Output light modification
 * @param keyName
 * 		The key name
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Light_ProcessRGBPropertyModificationAdd( const NParserOutputList *clientJsonData,
	__OUTPUT NHUELightModification *lightModification,
	const char *keyName );

#endif // !NDEVICEHUE_LIGHT_PROTECT
