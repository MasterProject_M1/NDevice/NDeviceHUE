#ifndef NDEVICEHUE_NDEVICEHUEAUTHENTICATION_PROTECT
#define NDEVICEHUE_NDEVICEHUEAUTHENTICATION_PROTECT

// -------------------------------------------
// struct NDeviceHUE::NDeviceHUEAuthentication
// -------------------------------------------

typedef struct NDeviceHUEAuthentication
{
	// Name
	char *m_name;

	// Max retry count
	NU32 m_maximumIDAuthenticationRetryCount;

	// Current retry count
	NU32 m_currentIDAuthenticationRetryCount;

	// Device thread ID
	NThread *m_threadIDObtention;

	// Authentication ID
	char *m_authenticationID;

	// IP
	const char *m_ip;
} NDeviceHUEAuthentication;

/**
 * Build HUE authentication instance
 *
 * @param name
 * 		The device name
 * @param ip
 * 		The device ip
 * @param maximumAuthenticationRetryCount
 * 		The maximum retry count (ignored if 0)
 *
 * @return the HUE authentication instance
 */
__ALLOC NDeviceHUEAuthentication *NDeviceHUE_NDeviceHUEAuthentication_Build( const char *name,
	const char *ip,
	NU32 maximumAuthenticationRetryCount );

/**
 * Destroy HUE authentication
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_NDeviceHUEAuthentication_Destroy( NDeviceHUEAuthentication** );

/**
 * Get hue device authentication ID
 *
 * @param this
 * 		This instance
 *
 * @return the id or NULL if not got yet
 */
const char *NDeviceHUE_NDeviceHUEAuthentication_GetAuthenticationID( const NDeviceHUEAuthentication* );

/**
 * Is device ready? (Is an ID got obtained)
 *
 * @param this
 * 		This instance
 *
 * @return if the device is ready
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_IsReady( const NDeviceHUEAuthentication* );

/**
 * Is authentication failed? (if retry count present)
 *
 * @param this
 * 		This instance
 *
 * @return if the authentication failed
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_IsAuthenticationFailed( const NDeviceHUEAuthentication* );

/**
 * Get the current retry count
 *
 * @param this
 * 		This instance
 *
 * @return the retry count
 */
NU32 NDeviceHUE_NDeviceHUEAuthentication_GetRetryCount( const NDeviceHUEAuthentication* );

/**
 * Get the device name
 *
 * @param this
 * 		This instance
 *
 * @return the name
 */
const char *NDeviceHUE_NDeviceHUEAuthentication_GetName( const NDeviceHUEAuthentication* );

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NDeviceHUE_NDeviceHUEAuthentication_GetIP( const NDeviceHUEAuthentication* );

/**
 * Save the information about device
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeedeed
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_Save( const NDeviceHUEAuthentication* );

/**
 * Build parser output list describing the current authentication state
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 * @param isAddRootKey
 * 		Do we add root key?
 *
 * @return the parser output list
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_BuildParserOutputList( const NDeviceHUEAuthentication*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot,
	NBOOL isAddRootKey );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param element
 * 		The element to be processed
 * @param cursor
 * 		The cursor
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_ProcessRESTGETRequest( const NDeviceHUEAuthentication *,
	__OUTPUT
	NParserOutputList *parserOutputList,
	const char *element,
	NU32 *cursor );

#endif // !NDEVICEHUE_NDEVICEHUEAUTHENTICATION_PROTECT
