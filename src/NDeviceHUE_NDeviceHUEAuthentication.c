#include "../include/NDeviceHUE.h"

/**
 * Receive callback
 *
 * @param packet
 *		The received packet
 * @param client
 * 		The client
 *
 * @return if the operation succeedeed
 */
__PRIVATE __CALLBACK NBOOL NDeviceHUE_NDeviceHUEAuthentication_CallbackReceiveAuthenticationID( const NPacket *packet,
	const NClient *client )
{
	// HUE device
	NDeviceHUEAuthentication *this;

	// HTTP client
	NClientHTTP *clientHTTP;

	// Answer
	NReponseHTTP *httpAnswer;

	// Secure answer text
	char *secureAnswerText;

	// JSon content
	NParserOutputList *jsonResult;

	// JSon output
	const NParserOutput *successOutput;

	// Reponse post text content
	char *responseContent;

	// Get http client
	if( !( clientHTTP = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		// Get device
		|| !( this = NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( clientHTTP ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Secure text
	if( !( secureAnswerText = NLib_Chaine_DupliquerSecurite( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// Build http answer
	if( !( httpAnswer = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( secureAnswerText ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( secureAnswerText );

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( secureAnswerText );

	// Get response content
	if( !( responseContent = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( httpAnswer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Destroy answer
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &httpAnswer );

		// Quit
		return NFALSE;
	}

	// Destroy answer
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &httpAnswer );

	// Parse
	if( !( jsonResult = NJson_Engine_Parser_Parse( responseContent,
		(NU32)strlen( responseContent ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( responseContent );

		// Quit
		return NFALSE;
	}

	// Free response content
	NFREE( responseContent );

	// Get the success key
	if( !( successOutput = NParser_Output_NParserOutputList_GetFirstOutput( jsonResult,
		NDEVICEHUE_JSON_SUCCESS_KEY_ID_CREATION,
		NTRUE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( successOutput ) != NPARSER_OUTPUT_TYPE_STRING )
	{
		// Ask for button pressure
		NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
			"Press HUE button!",
			0 );

		// Clean
		NParser_Output_NParserOutputList_Destroy( &jsonResult );

		// Quit
		return NFALSE;
	}

	// Clean id
	NFREE( this->m_authenticationID );

	// Save the id
	if( !( this->m_authenticationID = NLib_Chaine_Dupliquer( NParser_Output_NParserOutput_GetValue( successOutput ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Clean
		NParser_Output_NParserOutputList_Destroy( &jsonResult );

		// Quit
		return NFALSE;
	}

	// Destroy json result
	NParser_Output_NParserOutputList_Destroy( &jsonResult );

	// Write id
	NDeviceHUE_NDeviceHUEAuthentication_Save( this );

	// OK
	return NTRUE;
}

/**
 * ID obtention thread
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeedeed
 */
__PRIVATE __THREAD NBOOL NDeviceHUE_NDeviceHUEAuthentication_ThreadIDObtention( NDeviceHUEAuthentication *this )
{
	// HTTP client
	NClientHTTP *clientHTTP;

	// Request
	NRequeteHTTP *request;

	// Server IP
	const char *hostIP;

	// Build client
	if( !( clientHTTP = NHTTP_Client_NClientHTTP_Construire2( this->m_ip,
		NDEVICE_COMMON_TYPE_HUE_LISTENING_PORT,
		this,
		NDeviceHUE_NDeviceHUEAuthentication_CallbackReceiveAuthenticationID ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Request id
	do
	{
		// Build request
		if( ( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_POST,
			NDEVICEHUE_URL_TO_GET_NEW_ID ) ) != NULL )
		{
			// Default user
			NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( request,
				NDEVICEHUE_DEFAULT_DEVICE_USER,
				(NU32)strlen( NDEVICEHUE_DEFAULT_DEVICE_USER ) + 1 );

			// Add host information
			if( ( hostIP = NHTTP_Client_NClientHTTP_ObtenirIPServeur( clientHTTP ) ) != NULL )
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
					NMOT_CLEF_REQUETE_HTTP_KEYWORD_HOST,
					hostIP );

			// Send request
			NHTTP_Client_NClientHTTP_EnvoyerRequete( clientHTTP,
				request,
				0 );
		}

		// Wait
		NLib_Temps_Attendre( NDEVICEHUE_WAITING_TIME_BETWEEN_ID_REQUEST );

		// Increment attempts count
		this->m_currentIDAuthenticationRetryCount++;
	} while( this->m_authenticationID == NULL
		&& ( this->m_maximumIDAuthenticationRetryCount > 0 ?
			(NBOOL)( this->m_currentIDAuthenticationRetryCount < this->m_maximumIDAuthenticationRetryCount )
			: NTRUE ) );

	// Destroy client
	NHTTP_Client_NClientHTTP_Detruire( &clientHTTP );

	// Save the id
	NDeviceHUE_NDeviceHUEAuthentication_Save( this );

	// OK
	return NTRUE;
}

/**
 * Build HUE authentication instance
 *
 * @param name
 * 		The device name
 * @param ip
 * 		The device ip
 * @param maximumAuthenticationRetryCount
 * 		The maximum retry count (ignored if 0)
 *
 * @return the HUE authentication instance
 */
__ALLOC NDeviceHUEAuthentication *NDeviceHUE_NDeviceHUEAuthentication_Build( const char *name,
	const char *ip,
	NU32 maximumAuthenticationRetryCount )
{
	// Output
	__OUTPUT NDeviceHUEAuthentication *out;

	// Buffer
	char buffer[ 256 ];

	// Device file
	NFichierTexte *deviceFile;

	// Create database directory
	NLib_Module_Repertoire_CreerRepertoire( NDEVICEHUE_DATABASE_LOCATION );

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NDeviceHUEAuthentication ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_maximumIDAuthenticationRetryCount = maximumAuthenticationRetryCount;

	// Duplicate name
	if( !( out->m_name = NLib_Chaine_Dupliquer( name ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save IP
	out->m_ip = ip;

	// Build device ID filename
	snprintf( buffer,
		256,
		"%s/%s",
		NDEVICEHUE_DATABASE_LOCATION,
		out->m_name );

	// Load the device ID file
	if( ( deviceFile = NLib_Fichier_NFichierTexte_ConstruireLecture( buffer ) ) != NULL )
	{
		// Read the device ID file
		if( !( out->m_authenticationID = NLib_Fichier_NFichierTexte_LireMot( deviceFile,
			NFALSE ) ) )
			// Notify
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

		// Close the file
		NLib_Fichier_NFichierTexte_Detruire( &deviceFile );

		// Check id
		if( !out->m_authenticationID )
			// Remove the file
			NLib_Fichier_Operation_SupprimerFichier( buffer );
	}

	// Did the file read method failed?
	if( !out->m_authenticationID )
		// Create thread
		if( !( out->m_threadIDObtention = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NDeviceHUE_NDeviceHUEAuthentication_ThreadIDObtention,
			out,
			NULL ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Free
			NFREE( out->m_name );
			NFREE( out );

			// Quit
			return NULL;
		}

	// OK
	return out;
}

/**
 * Destroy HUE authentication
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_NDeviceHUEAuthentication_Destroy( NDeviceHUEAuthentication **this )
{
	// Destroy thread
	if( (*this)->m_threadIDObtention != NULL )
		// Destroy
		NLib_Thread_NThread_Detruire( &(*this)->m_threadIDObtention );

	// Free
	NFREE( (*this)->m_name );
	NFREE( (*this)->m_authenticationID );
	NFREE( (*this) );
}

/**
 * Is device ready? (Is an ID got obtained)
 *
 * @param this
 * 		This instance
 *
 * @return if the device is ready
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_IsReady( const NDeviceHUEAuthentication *this )
{
	return (NBOOL)( this->m_authenticationID != NULL );
}

/**
 * Is authentication failed? (if retry count present)
 *
 * @param this
 * 		This instance
 *
 * @return if the authentication failed
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_IsAuthenticationFailed( const NDeviceHUEAuthentication *this )
{
	return (NBOOL)( this->m_authenticationID == NULL
		&& ( this->m_maximumIDAuthenticationRetryCount > 0 ?
			(NBOOL)( this->m_currentIDAuthenticationRetryCount >= this->m_maximumIDAuthenticationRetryCount )
			: NFALSE ) );
}

/**
 * Get the current retry count
 *
 * @param this
 * 		This instance
 *
 * @return the retry count
 */
NU32 NDeviceHUE_NDeviceHUEAuthentication_GetRetryCount( const NDeviceHUEAuthentication *this )
{
	return this->m_currentIDAuthenticationRetryCount;
}

/**
 * Get the device name
 *
 * @param this
 * 		This instance
 *
 * @return the name
 */
const char *NDeviceHUE_NDeviceHUEAuthentication_GetName( const NDeviceHUEAuthentication *this )
{
	return this->m_name;
}

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NDeviceHUE_NDeviceHUEAuthentication_GetIP( const NDeviceHUEAuthentication *this )
{
	return this->m_ip;
}

/**
 * Get hue device authentication ID
 *
 * @param this
 * 		This instance
 *
 * @return the id or NULL if not got yet
 */
const char *NDeviceHUE_NDeviceHUEAuthentication_GetAuthenticationID( const NDeviceHUEAuthentication *this )
{
	return this->m_authenticationID;
}

/**
 * Save the information about device
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeedeed
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_Save( const NDeviceHUEAuthentication *this )
{
	// Text file
	NFichierTexte *file;

	// Buffer
	char buffer[ 256 ];

	// Check current state
	if( !this->m_authenticationID )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Build file path
	snprintf( buffer,
		256,
		"%s/%s",
		NDEVICEHUE_DATABASE_LOCATION,
		this->m_name );

	// Open file
	if( !( file = NLib_Fichier_NFichierTexte_ConstruireEcriture( buffer,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quit
		return NFALSE;
	}

	// Write
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_authenticationID );

	// Close file
	NLib_Fichier_NFichierTexte_Detruire( &file );

	// OK
	return NTRUE;
}

/**
 * Build parser output list (internal)
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output
 * @param keyRoot
 * 		The key root
 * @param serviceType
 * 		The service type
 * @param isAddRootKey
 * 		Do we add the authentication root key?
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NDeviceHUE_NDeviceHUEAuthentication_BuildParserOutputListInternal( const NDeviceHUEAuthentication *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot,
	NDeviceHUEAuthenticationServiceType serviceType,
	NBOOL isAddRootKey )
{
	// Key
	char key[ 512 ];

	switch( serviceType )
	{
		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_IP:
			// Build key
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				strlen( keyRoot ) > 0 ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_GetJsonKey( NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_IP,
					isAddRootKey ) );

			// Add entry
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_ip );
			break;

		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_API_KEY:
			// API key
			if( NDeviceHUE_NDeviceHUEAuthentication_IsReady( this ) )
			{
				snprintf( key,
					512,
					"%s%s%s",
					keyRoot,
					strlen( keyRoot ) > 0 ?
						"."
						: "",
					NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_GetJsonKey( NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_API_KEY,
						isAddRootKey ) );
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_authenticationID );
			}
			break;
		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_RETRY_COUNT:
			// Authentication retry count
			if( !NDeviceHUE_NDeviceHUEAuthentication_IsReady( this ) )
			{
				snprintf( key,
					512,
					"%s%s%s",
					keyRoot,
					strlen( keyRoot ) > 0 ?
						"."
						: "",
					NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_GetJsonKey( NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_RETRY_COUNT,
						isAddRootKey ) );
				NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
					key,
					this->m_currentIDAuthenticationRetryCount );
			}
			break;
		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_IS_AUTHENTICATED:
			// Is authenticated
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				strlen( keyRoot ) > 0 ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_GetJsonKey( NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_IS_AUTHENTICATED,
					isAddRootKey ) );
			NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				NDeviceHUE_NDeviceHUEAuthentication_IsReady( this ) );
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list describing the current authentication state
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 * @param isAddRootKey
 * 		Do we add root key?
 *
 * @return the parser output list
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_BuildParserOutputList( const NDeviceHUEAuthentication *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot,
	NBOOL isAddRootKey )
{
	// Iterator
	NDeviceHUEAuthenticationServiceType i = (NDeviceHUEAuthenticationServiceType)0;

	// Add
	for( ; i < NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES; i++ )
		NDeviceHUE_NDeviceHUEAuthentication_BuildParserOutputListInternal( this,
			parserOutputList,
			keyRoot,
			i,
			isAddRootKey );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param element
 * 		The element to be processed
 * @param cursor
 * 		The cursor
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_NDeviceHUEAuthentication_ProcessRESTGETRequest( const NDeviceHUEAuthentication *this,
	__OUTPUT
	NParserOutputList *parserOutputList,
	const char *element,
	NU32 *cursor )
{
	// Service
	NDeviceHUEAuthenticationServiceType serviceType;

	// Service type
	switch( ( serviceType = NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_FindService( element,
		cursor ) ) )
	{
		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_ROOT:
			return NDeviceHUE_NDeviceHUEAuthentication_BuildParserOutputList( this,
				parserOutputList,
				"",
				NFALSE );

		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_IP:
		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_IS_AUTHENTICATED:
		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_RETRY_COUNT:
		case NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_API_KEY:
			return NDeviceHUE_NDeviceHUEAuthentication_BuildParserOutputListInternal( this,
				parserOutputList,
				"",
				serviceType,
				NFALSE );

		default:
			return NFALSE;
	}
}

