#include "../../include/NDeviceHUE.h"

// -----------------------------------
// struct NDeviceHUE::Light::NHUELight
// -----------------------------------

/**
 * Build a light
 *
 * @param lightInformation
 * 		The light information details
 * @param lightType
 * 		The light type
 * @param authentication
 * 		The hue device authentication
 * @param allLightInformation
 * 		The json parsed data
 *
 * @return the light instance
 */
__ALLOC NHUELight *NDeviceHUE_Light_NHUELight_Build( const char *lightInformation,
	const char *lightType,
	const NDeviceHUEAuthentication *authentication,
	const NParserOutputList *allLightInformation )
{
	// Output
	__OUTPUT NHUELight *out;

	// Buffer
	char buffer[ 2048 ];

	// Cursor
	NU32 cursor = 0;

	// Parser output
	const NParserOutput *parserOutput;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NHUELight ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Parse type
	if( ( out->m_type = NDeviceHUE_Light_NHUELightType_FindLightType( lightType ) ) >= NHUE_LIGHT_TYPES )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Extract id
	if( ( out->m_id = NLib_Chaine_LireNombreNonSigne( lightInformation,
		&cursor,
		NFALSE ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build name key
	snprintf( buffer,
		2048,
		"%d.%s",
		out->m_id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_NAME ) );

	// Find name
	if( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( allLightInformation,
		buffer,
		NTRUE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save name
	if( !( out->m_name = NLib_Chaine_Dupliquer( NParser_Output_NParserOutput_GetValue( parserOutput ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save authentication
	out->m_authentication = authentication;

	// Build API request link
	snprintf( buffer,
		2048,
		NDEVICEHUE_LIGHT_STATE_API_REQUEST,
		NDeviceHUE_NDeviceHUEAuthentication_GetAuthenticationID( out->m_authentication ),
		out->m_id );

	// Save API request link
	if( !( out->m_apiLightStateRequestLink = NLib_Chaine_Dupliquer( buffer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_name );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build http client
	if( !( out->m_client = NHTTP_Client_NClientHTTP_Construire2( NDeviceHUE_NDeviceHUEAuthentication_GetIP( out->m_authentication ),
		NDEVICE_COMMON_TYPE_HUE_LISTENING_PORT,
		out,
		NULL ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_apiLightStateRequestLink );
		NFREE( out->m_name );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build current state
	if( !( out->m_state = NDeviceHUE_Light_NHUELightState_Build( allLightInformation,
		out->m_id ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Destroy http client
		NHTTP_Client_NClientHTTP_Detruire( &out->m_client );

		// Free
		NFREE( out->m_apiLightStateRequestLink );
		NFREE( out->m_name );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy a light
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Light_NHUELight_Destroy( NHUELight **this )
{
	// Destroy client
	NHTTP_Client_NClientHTTP_Detruire( &(*this)->m_client );

	// Destroy state
	NDeviceHUE_Light_NHUELightState_Destroy( &(*this)->m_state );

	// Free
	NFREE( (*this)->m_apiLightStateRequestLink );
	NFREE( (*this)->m_name );
	NFREE( (*this) );
}

/**
 * Send a request
 *
 * @param this
 * 		This instance
 * @param httpRequestType
 * 		The http request type
 * @param apiElement
 * 		The api element to request
 * @param jsonRequest
 * 		The request to send
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_SendRequest( const NHUELight *this,
	NTypeRequeteHTTP httpRequestType,
	const char *apiElement,
	const char *jsonRequest )
{
	// HTTP request
	NRequeteHTTP *request;

	// IP
	const char *hostIP;

	// Build request
	if( ( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( httpRequestType,
		apiElement ) ) != NULL )
	{
		// Light detail
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( request,
			jsonRequest,
			(NU32)strlen( jsonRequest ) );

		// Add host information
		if( ( hostIP = NHTTP_Client_NClientHTTP_ObtenirIPServeur( this->m_client ) ) != NULL )
			NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
				NMOT_CLEF_REQUETE_HTTP_KEYWORD_HOST,
				hostIP );

		// Send request
		return NHTTP_Client_NClientHTTP_EnvoyerRequete( this->m_client,
			request,
			NDEVICEHUE_LIGHT_REQUEST_TIMEOUT );
	}

	// Error
	return NFALSE;
}

/**
 * Send a request
 *
 * @param this
 * 		This instance
 * @param httpRequestType
 * 		The http request type
 * @param apiElement
 * 		The api element to request
 * @param jsonRequest
 * 		The request to send
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_SendRequest2( const NHUELight *this,
	NTypeRequeteHTTP httpRequestType,
	const char *jsonRequest )
{
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		httpRequestType,
		this->m_apiLightStateRequestLink,
		jsonRequest );
}

/**
 * Send root request
 *
 * @param this
 * 		This instance
 * @param httpRequestType
 * 		The http request type
 * @param jsonRequest
 * 		The json request to be done
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_SendRootRequest( const NHUELight *this,
	NTypeRequeteHTTP httpRequestType,
	const char *jsonRequest )
{
	// Element
	char apiElement[ 2048 ];

	// Build element
	snprintf( apiElement,
		2048,
		NDEVICEHUE_LIGHT_API_REQUEST,
		NDeviceHUE_NDeviceHUEAuthentication_GetAuthenticationID( this->m_authentication ),
		this->m_id );

	// Send request
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		httpRequestType,
		apiElement,
		jsonRequest );
}

/**
 * Get light id
 *
 * @param this
 * 		This instance
 *
 * @return the light id
 */
NU32 NDeviceHUE_Light_NHUELight_GetID( const NHUELight *this )
{
	return this->m_id;
}

/**
 * Get light type
 *
 * @param this
 * 		This instance
 *
 * @return the type
 */
NHUELightType NDeviceHUE_Light_NHUELight_GetType( const NHUELight *this )
{
	return this->m_type;
}

/**
 * Get light name
 *
 * @param this
 * 		This instance
 *
 * @return the light name
 */
const char *NDeviceHUE_Light_NHUELight_GetName( const NHUELight *this )
{
	return this->m_name;
}

/**
 * Get state
 *
 * @param this
 * 		This instance
 *
 * @return the last known light state
 */
const NHUELightState *NDeviceHUE_Light_NHUELight_GetState( const NHUELight *this )
{
	return this->m_state;
}

/**
 * Activate a light
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ActivateLight( NHUELight *this )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":true}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_IS_ON ) );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Deactivate a light
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_DeactivateLight( NHUELight *this )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":false}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_IS_ON ) );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Short blink a light (will shutdown the light after blink)
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ShortBlinkLight( NHUELight *this )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":\"%s\", \"%s\":false}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_ALERT ),
		NDeviceHUE_Light_NHUELightAlert_GetName( NHUE_LIGHT_ALERT_SELECT ),
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_IS_ON ) );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Blink a light
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_BlinkLight( NHUELight *this )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":\"%s\"}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_ALERT ),
		NDeviceHUE_Light_NHUELightAlert_GetName( NHUE_LIGHT_ALERT_SELECT ) );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Long blink a light (for 15 seconds, with a period of 1 second)
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_LongBlinkLight( NHUELight *this )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":\"%s\"}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_ALERT ),
		NDeviceHUE_Light_NHUELightAlert_GetName( NHUE_LIGHT_ALERT_LONG_SELECT ) );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Activate color loop
 *
 * @param this
 * 		This instance
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ActivateColorLoop( NHUELight *this )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":\"%s\"}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_EFFECT ),
		NDeviceHUE_Light_NHUELightEffect_GetName( NHUE_LIGHT_EFFECT_COLOR_LOOP ) );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Deactivate color loop
 *
 * @param this
 * 		This instance
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_DeactivateColorLoop( NHUELight *this )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":\"%s\"}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_EFFECT ),
		NDeviceHUE_Light_NHUELightEffect_GetName( NHUE_LIGHT_EFFECT_NONE ) );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Change light intensity
 *
 * @param this
 * 		This instance
 * @param brightness
 * 		The light intensity (1-254)
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeIntensity( NHUELight *this,
	NU8 brightness )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":%d}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_BRIGHTNESS ),
		(NU32)brightness );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Change color and intensity
 *
 * @param this
 * 		This instance
 * @param hue
 * 		The hue value (0 to 65535)
 * @param saturation
 * 		The saturation value (0 to 255)
 * @param brightness
 * 		The brightness (0 to 255)
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColorAndIntensity( NHUELight *this,
	NU16 hue,
	NU8 saturation,
	NU8 brightness )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":%d, \"%s\":%d, \"%s\":%d}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_HUE ),
		(NU32)hue,
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_SATURATION ),
		(NU32)saturation,
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_BRIGHTNESS ),
		brightness );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Change light color with hue/saturation combination
 *
 * @param this
 * 		This instance
 * @param hue
 * 		The hue value (0 to 65535)
 * @param saturation
 * 		The saturation value
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor( NHUELight *this,
	NU16 hue,
	NU8 saturation )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":%d, \"%s\":%d}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_HUE ),
		(NU32)hue,
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_SATURATION ),
		(NU32)saturation );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Change light color with xy combination
 *
 * @param this
 * 		This instance
 * @param x
 * 		The x color coordinate
 * @param y
 * 		The y color coordinate
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor2( NHUELight *this,
	double x,
	double y )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\": [ %f, %f ]}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_XY ),
		x,
		y );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Change light color with color temperature
 *
 * @param this
 * 		This instance
 * @param colorTemperature
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor3( NHUELight *this,
	NU16 colorTemperature )
{
	// Buffer
	char buffer[ 2048 ];

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":%d}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_TEMPERATURE ),
		(NU32)colorTemperature );

	// Execute
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		this->m_apiLightStateRequestLink,
		buffer );
}

/**
 * Change color with rgb value
 *
 * @param this
 * 		This instance
 * @param r
 * 		The red value
 * @param g
 * 		The green value
 * @param b
 * 		The blue value
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor4( NHUELight *this,
	NU8 r,
	NU8 g,
	NU8 b )
{
	// Values
	NU16 hue;
	NU8 saturation,
		brightness;

	// Convert
	NDeviceHUE_Light_ConvertRGBToHUESaturationBrightness( r,
		g,
		b,
		&hue,
		&saturation,
		&brightness );

	// Change color
	return (NBOOL)( NDeviceHUE_Light_NHUELight_ChangeColorAndIntensity( this,
		hue,
		saturation,
		brightness ) );
}

/**
 * Change color with rgb value
 *
 * @param this
 * 		This instance
 * @param color
 * 		The rgb color
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeColor5( NHUELight *this,
	NCouleur color )
{
	return NDeviceHUE_Light_NHUELight_ChangeColor4( this,
		color.r,
		color.g,
		color.b );
}

/**
 * Set the light name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The new light name
 *
 * @return if operation succedeed
 */
NBOOL NDeviceHUE_Light_NHUELight_ChangeName( NHUELight *this,
	const char *name )
{
	// Element
	char apiElement[ 2048 ];

	// Buffer
	char buffer[ 2048 ];

	// Build element
	snprintf( apiElement,
		2048,
		NDEVICEHUE_LIGHT_API_REQUEST,
		NDeviceHUE_NDeviceHUEAuthentication_GetAuthenticationID( this->m_authentication ),
		this->m_id );

	// Build request
	snprintf( buffer,
		2048,
		"{\"%s\":\"%s\"}",
		NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUE_LIGHT_PROPERTY_NAME ),
		name );

	// Send request
	return NDeviceHUE_Light_NHUELight_SendRequest( this,
		NTYPE_REQUETE_HTTP_PUT,
		apiElement,
		buffer );
}

/**
 * Build parser output list (private)
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service to add
 * @param keyStartToRemove
 * 		The key start removal part (or NULL)
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NDeviceHUE_Light_NHUELight_BuildParserOutputListInternal( const NHUELight *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NDeviceHUELightServiceType serviceType,
	const char *keyStartToRemove )
{
	// Key
	char key[ 512 ];

	// Display separator
	NBOOL isDisplaySeparator;

	// Root type
	NDeviceHUELightServiceType rootType;

	// Set separator display
	isDisplaySeparator = (NBOOL)( strlen( keyRoot ) > 0 );

	switch( serviceType )
	{
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_NAME:
			// Add name
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_NAME,
					keyStartToRemove ) );
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_name );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_TYPE:
			// Add type
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_TYPE,
					keyStartToRemove ) );
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NDeviceHUE_Light_NHUELightType_GetLightName( this->m_type ) );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_ID:
			// Add id
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_ID,
					keyStartToRemove ) );
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_id );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_ON:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_REACHABLE:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT:
			// Find root type
			switch( serviceType )
			{
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR:
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS:
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION:
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE:
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
					rootType = NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR;
					break;

				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT:
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT:
					rootType = NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC;
					break;

				default:
					rootType = NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT;
					break;
			}

			// Add key
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( rootType,
					keyStartToRemove ) );

			return NDeviceHUE_Light_NHUELightState_BuildParserOutputListByService( this->m_state,
				key,
				parserOutputList,
				serviceType );

		default:
			break;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_BuildParserOutputList( const NHUELight *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service
	NDeviceHUELightServiceType serviceType = (NDeviceHUELightServiceType)0;

	// Iterate services
	for( ; serviceType < NDEVICE_HUE_LIGHT_SERVICE_TYPES; serviceType++ )
		if( !NDeviceHUE_Light_NHUELight_BuildParserOutputListInternal( this,
			keyRoot,
			parserOutputList,
			serviceType,
			NULL ) )
			return NFALSE;

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor into element
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_ProcessRESTGetRequest( const NHUELight *this,
	NParserOutputList *parserOutputList,
	const char *element,
	NU32 *cursor )
{
	// Service type
	NDeviceHUELightServiceType serviceType;

	// Find service type
	switch( ( serviceType = NDeviceHUE_Service_NDeviceHUELightServiceType_FindService( element,
		'/',
		*cursor ) ) )
	{
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_ROOT:
			return NDeviceHUE_Light_NHUELight_BuildParserOutputList( this,
				"",
				parserOutputList );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_NAME:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_TYPE:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_ID:
			return NDeviceHUE_Light_NHUELight_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType,
				NULL );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_ON:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_REACHABLE:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT:
			return NDeviceHUE_Light_NHUELight_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType,
				NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"." );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
			return NDeviceHUE_Light_NHUELight_BuildParserOutputListInternal( this,
				"",
				parserOutputList,
				serviceType,
				NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"."NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_ROOT"." );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT:
			return NDeviceHUE_Light_NHUELightState_BuildParserOutputList( this->m_state,
				parserOutputList );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR:
			return (NBOOL)( NDeviceHUE_Light_NHUELightState_BuildParserOutputListByService( this->m_state,
				"",
				parserOutputList,
				NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS )
					&& NDeviceHUE_Light_NHUELightState_BuildParserOutputListByService( this->m_state,
						"",
						parserOutputList,
						NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION )
					&& NDeviceHUE_Light_NHUELightState_BuildParserOutputListByService( this->m_state,
						"",
						parserOutputList,
						NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE )
					&& NDeviceHUE_Light_NHUELightState_BuildParserOutputListByService( this->m_state,
						"",
						parserOutputList,
						NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB ) );

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Add property to modification list (private)
 *
 * @param this
 * 		This instance
 * @param modification
 * 		The modification list
 * @param serviceType
 * 		The service type
 * @param clientDataJson
 * 		The client request data
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NDeviceHUE_Light_NHUELight_AddPropertyToModificationList( NHUELight *this,
	__OUTPUT NHUELightModification *modification,
	NDeviceHUELightServiceType serviceType,
	const NParserOutputList *clientDataJson )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Parser output
	const NParserOutput *parserOutput;

	// Is RGB color processed?
	NBOOL isRGBColorProcessed = NFALSE;

	// Iterator
	NU32 i;

	// Reference
	NREFERENCER( this );

	// Look for requested properties
	if( !( parserOutputList = NParser_Output_NParserOutputList_GetAllOutput( clientDataJson,
		NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( serviceType ),
		NTRUE,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Iterate
	for( i = 0; i < NParser_Output_NParserOutputList_GetEntryCount( parserOutputList ); i++ )
		// Get entry
		if( ( parserOutput = NParser_Output_NParserOutputList_GetEntry( parserOutputList,
			i ) ) != NULL )
			switch( serviceType )
			{
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
					// Check if already processed
					if( isRGBColorProcessed )
						break;

					// Process
					NDeviceHUE_Light_ProcessRGBPropertyModificationAdd( parserOutputList,
						modification,
						NParser_Output_NParserOutput_GetKey( parserOutput ) );

					// Done
					isRGBColorProcessed = NTRUE;
					break;

				default:
					// Add property
					if( !NDeviceHUE_Service_PUT_NHUELightModification_AddProperty( modification,
						serviceType,
						parserOutput ) )
					{
						// Notify
						NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

						// Destroy
						NParser_Output_NParserOutputList_Destroy( &parserOutputList );

						// Quit
						return NFALSE;
					}
					break;
			}

	// Destroy
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// OK
	return NTRUE;
}

/**
 * Find modification in root list (private)
 *
 * @param this
 * 		This instance
 * @param modification
 * 		The modification list to apply
 * @param clientDataJson
 * 		The client data
 * @param isRootModification
 * 		Do we modificate the root api or state?
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NDeviceHUE_Light_NHUELight_FindModificationRoot( NHUELight *this,
	__OUTPUT NHUELightModification *modification,
	const NParserOutputList *clientDataJson,
	__OUTPUT NBOOL *isRootModification )
{
	// Parser entry
	const NParserOutput *entry;

	// RGB color processed?
	NBOOL isRGBColorProcessed = NFALSE;

	// Service type
	NDeviceHUELightServiceType serviceType;

	// Iterator
	NU32 i = 0;

	// Iterate client data
	for( ; i < NParser_Output_NParserOutputList_GetEntryCount( clientDataJson ); i++ )
		// Get entry
		if( ( entry = NParser_Output_NParserOutputList_GetEntry( clientDataJson,
			i ) ) != NULL )
		{
			// Find service type
			if( ( serviceType = NDeviceHUE_Service_NDeviceHUELightServiceType_FindService( NParser_Output_NParserOutput_GetKey( entry ),
				'.',
				0 ) ) >= NDEVICE_HUE_LIGHT_SERVICE_TYPES )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_SYNTAX );

				// Quit
				return NFALSE;
			}

			// Check service type
			switch( serviceType )
			{
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_NAME:
					*isRootModification = NTRUE;
					break;
				case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
					// Already processed RGB data?
					if( isRGBColorProcessed )
						break;

					// Process RGB color
					NDeviceHUE_Light_ProcessRGBPropertyModificationAdd( clientDataJson,
						modification,
						NParser_Output_NParserOutput_GetKey( entry ) );

					// Done
					isRGBColorProcessed = NTRUE;
					break;

				default:
					// Add property
					if( !NDeviceHUE_Service_PUT_NHUELightModification_AddProperty( modification,
						serviceType,
						entry ) )
					{
						// Notify
						NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

						// Quit
						return NFALSE;
					}
					break;
			}
		}

	// OK
	return NTRUE;
}

/**
 * Process REST PUT request
 *
 * @param this
 * 		This instance
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor
 * @param clientDataJson
 * 		The client parsed data
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELight_ProcessRESTPUTRequest( NHUELight *this,
	const char *element,
	NU32 cursor,
	const NParserOutputList *clientDataJson )
{
	// Return code
	__OUTPUT NBOOL returnCode = NFALSE;

	// Is root modification?
	NBOOL isRootModification = NFALSE;

	// Modification list
	NHUELightModification *modification;

	// Service type
	NDeviceHUELightServiceType serviceType;

	// Create modification list
	if( !( modification = NDeviceHUE_Service_PUT_NHUELightModification_Build( this ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Find service
	switch( ( serviceType = NDeviceHUE_Service_NDeviceHUELightServiceType_FindService( element,
		'/',
		cursor ) ) )
	{
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_ROOT:
			// Find properties
			returnCode = NDeviceHUE_Light_NHUELight_FindModificationRoot( this,
				modification,
				clientDataJson,
				&isRootModification );
			break;

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_NAME:
			isRootModification = NTRUE;

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_ON:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT:
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT:
			// Add property
			returnCode = NDeviceHUE_Light_NHUELight_AddPropertyToModificationList( this,
				modification,
				serviceType,
				clientDataJson );
			break;

		default:
			break;
	}

	// Do we have modifications?
	if( returnCode != NFALSE
		&& NDeviceHUE_Service_PUT_NHUELightModification_IsModification( modification ) )
		// Apply modification
		returnCode = NDeviceHUE_Service_PUT_NHUELightModification_SendModification( modification,
			(NBOOL)( !isRootModification ) );

	// Destroy modification list
	NDeviceHUE_Service_PUT_NHUELightModification_Destroy( &modification );

	// OK?
	return returnCode;
}

