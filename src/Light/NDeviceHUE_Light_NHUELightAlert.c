#define NDEVICEHUE_LIGHT_NHUELIGHTALERT_INTERNE
#include "../../include/NDeviceHUE.h"

// --------------------------------------
// enum NDeviceHUE::Light::NHUELightAlert
// --------------------------------------

/**
 * Parser light alert
 *
 * @param alert
 * 		The alert
 *
 * @return the light alert
 */
NHUELightAlert NDeviceHUE_Light_NHUELightAlert_Parse( const char *alert )
{
	// Output
	__OUTPUT NHUELightAlert out = (NHUELightAlert)0;

	// Look for
	for( ; out < NHUE_LIGHT_ALERTS; out++ )
		// Check
		if( NLib_Chaine_Comparer( alert,
			NHUELightAlertName[ out ],
			NTRUE,
			0 ) )
			// Found it
			return out;

	// Not found
	return NHUE_LIGHT_ALERTS;
}

/**
 * Get the light alert name
 *
 * @param alert
 * 		The alert
 *
 * @return the light alert name
 */
const char *NDeviceHUE_Light_NHUELightAlert_GetName( NHUELightAlert alert )
{
	return NHUELightAlertName[ alert ];
}

