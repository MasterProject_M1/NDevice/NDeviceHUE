#define NDEVICEHUE_LIGHT_NHUELIGHTEFFECT_INTERNE
#include "../../include/NDeviceHUE.h"

// ---------------------------------------
// enum NDeviceHUE::Light::NHUELightEffect
// ---------------------------------------

/**
 * Parser light effect
 *
 * @param effect
 * 		The light effect
 *
 * @return the light effect
 */
NHUELightEffect NDeviceHUE_Light_NHUELightEffect_Parse( const char *effect )
{
	// Output
	NHUELightEffect out = (NHUELightEffect)0;

	// Look for
	for( ; out < NHUE_LIGHT_EFFECTS; out++ )
		// Check
		if( NLib_Chaine_Comparer( effect,
			NHUELightEffectName[ out ],
			NTRUE,
			0 ) )
			return out;

	// Not found
	return NHUE_LIGHT_EFFECTS;
}

/**
 * Get light effect name
 *
 * @param effect
 * 		The light effect
 *
 * @return the light effect name
 */
const char *NDeviceHUE_Light_NHUELightEffect_GetName( NHUELightEffect effect )
{
	return NHUELightEffectName[ effect ];
}

