#define NDEVICEHUE_LIGHT_NHUELIGHTTYPE_INTERNE
#include "../../include/NDeviceHUE.h"

// -------------------------------------
// enum NDeviceHUE::Light::NHUELightType
// -------------------------------------

/**
 * Find light type
 *
 * @param technicalID
 * 		The light technical id
 *
 * @return the light type or NHUE_LIGHT_TYPES if not found
 */
NHUELightType NDeviceHUE_Light_NHUELightType_FindLightType( const char *technicalID )
{
	// Iterator
	__OUTPUT NHUELightType out = (NHUELightType)0;

	// Look for type
	for( ; out < NHUE_LIGHT_TYPES; out++ )
		if( NLib_Chaine_Comparer( NHUELightTypeTechnicalID[ out ],
			technicalID,
			NTRUE,
			0 ) )
			return out;

	// Not found
	return NHUE_LIGHT_TYPES;
}

/**
 * Get light name
 *
 * @param lightType
 * 		The light type
 *
 * @return the light name
 */
const char *NDeviceHUE_Light_NHUELightType_GetLightName( NHUELightType lightType )
{
	return NHUELightTypeName[ lightType ];
}

