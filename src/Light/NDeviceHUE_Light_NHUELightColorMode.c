#define NDEVICEHUE_LIGHT_NHUELIGHTCOLORMODE_INTERNE
#include "../../include/NDeviceHUE.h"

// ------------------------------------------
// enum NDeviceHUE::Light::NHUELightColorMode
// ------------------------------------------

/**
 * Parser light color mode
 *
 * @param mode
 * 		The light color mode
 *
 * @return the light color mode
 */
NHUELightColorMode NDeviceHUE_Light_NHUELightColorMode_Parse( const char *mode )
{
	// Output
	__OUTPUT NHUELightColorMode out = (NHUELightColorMode)0;

	// Look for
	for( ; out < NHUE_LIGHT_COLOR_MODES; out++ )
		// Check
		if( NLib_Chaine_Comparer( mode,
			NHUELightColorModeName[ out ],
			NTRUE,
			0 ) )
			// Found it!
			return out;

	// Not found
	return NHUE_LIGHT_COLOR_MODES;
}

/**
 * Get light color mode name
 *
 * @param mode
 * 		The light color mode
 *
 * @return the light color mode name
 */
const char *NDeviceHUE_Light_NHUELightColorMode_GetName( NHUELightColorMode mode )
{
	return NHUELightColorModeName[ mode ];
}

