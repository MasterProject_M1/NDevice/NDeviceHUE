#define NDEVICEHUE_LIGHT_NHUELIGHTCOLOR_INTERNE
#include "../../include/NDeviceHUE.h"

// --------------------------------------
// enum NDeviceHUE::Light::NHUELightColor
// --------------------------------------

/**
 * Get values for color name
 *
 * @param colorName
 * 		The color name
 *
 * @return the RGB array values
 */
const NU8 *NDeviceHUE_Light_NHUELightColor_FindValueFromName( const char *colorName )
{
	// Iterator
	NHUELightColor i = (NHUELightColor)0;

	// Look for
	for( ; i < NHUE_LIGHT_COLORS; i++ )
		// Check name
		if( NLib_Chaine_Comparer( colorName,
			NHUELightColorName[ i ],
			NFALSE,
			0 ) )
			// Fount it
			return NHUELightColorValue[ i ];

	// Not found
	return NULL;
}
