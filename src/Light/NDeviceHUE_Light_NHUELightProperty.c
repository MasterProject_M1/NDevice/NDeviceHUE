#define NDEVICEHUE_LIGHT_NHUELIGHTPROPERTY_INTERNE
#include "../../include/NDeviceHUE.h"

// -----------------------------------------
// enum NDeviceHUE::Light::NHUELightProperty
// -----------------------------------------

/**
 * Get the property name
 *
 * @param property
 * 		The property
 *
 * @return the parser compatible name
 */
const char *NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUELightProperty property )
{
	return NHUELightPropertyJson[ property ];
}

/**
 * Get short state property
 *
 * @param property
 * 		The state property
 *
 * @return the short json property
 */
const char *NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( NHUELightProperty property )
{
	return NHUEStateLightPropertyJson[ property ];
}

