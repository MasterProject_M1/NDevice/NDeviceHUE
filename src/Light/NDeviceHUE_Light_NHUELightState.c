#include "../../include/NDeviceHUE.h"

// ----------------------------------------
// struct NDeviceHUE::Light::NHUELightState
// ----------------------------------------

/**
 * Build light state
 *
 * @param lightDetail
 * 		The lights details
 * @param id
 * 		The light id
 *
 * @return the built instance
 */
__ALLOC NHUELightState *NDeviceHUE_Light_NHUELightState_Build( const NParserOutputList *lightDetail,
	NU32 id )
{
	// Output
	__OUTPUT NHUELightState *out;

	// Buffer
	char buffer[ 256 ];

	// Value
	const NParserOutput *value;

	// XY values
	NParserOutputList *xyValue;

	// Allocate
	if( !( out = calloc( 1,
		sizeof( NHUELightState ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	/* Is light on? */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_IS_ON ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_isOn = *(NBOOL*)( NParser_Output_NParserOutput_GetValue( value ) );

	/* Is light reachable? */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_IS_REACHABLE ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_isReachable = *(NBOOL*)( NParser_Output_NParserOutput_GetValue( value ) );

	/* Hue */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_HUE ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_hue = (NU16)( *(NS32*)( NParser_Output_NParserOutput_GetValue( value ) ) );

	/* Saturation */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_SATURATION ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_saturation = (NU8)( *(NS32*)( NParser_Output_NParserOutput_GetValue( value ) ) );

	/* Brightness */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_BRIGHTNESS ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_brightness = (NU8)( *(NS32*)( NParser_Output_NParserOutput_GetValue( value ) ) );

	/* XY coordinate */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_XY ) );

	// Get data
	if( ( xyValue = NParser_Output_NParserOutputList_GetAllOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
	{
		// Get x
		if( ( value = NParser_Output_NParserOutputList_GetEntry( xyValue,
			0 ) ) != NULL )
			out->m_xy.x = *(double*)( NParser_Output_NParserOutput_GetValue( value ) );

		// Get y
		if( ( value = NParser_Output_NParserOutputList_GetEntry( xyValue,
			1 ) ) != NULL )
			out->m_xy.y = *(double*)( NParser_Output_NParserOutput_GetValue( value ) );

		// Clear data
		NParser_Output_NParserOutputList_Destroy( &xyValue );
	}

	/* Color temperature */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_TEMPERATURE ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_colorTemperature = (NU32)( *(NS32*)( NParser_Output_NParserOutput_GetValue( value ) ) );

	/* Light effect */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_EFFECT ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_effect = NDeviceHUE_Light_NHUELightEffect_Parse( NParser_Output_NParserOutput_GetValue( value ) );

	/* Alert */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_ALERT ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_alert = NDeviceHUE_Light_NHUELightAlert_Parse( NParser_Output_NParserOutput_GetValue( value ) );

	/* Color mode */
	// Compose buffer
	snprintf( buffer,
		256,
		"%d.%s",
		id,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_STATE_COLOR_MODE ) );

	// Save value
	if( ( value = NParser_Output_NParserOutputList_GetFirstOutput( lightDetail,
		buffer,
		NTRUE,
		NTRUE ) ) != NULL )
		out->m_colorMode = NDeviceHUE_Light_NHUELightColorMode_Parse( NParser_Output_NParserOutput_GetValue( value ) );

	// OK
	return out;
}

/**
 * Destroy instance
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Light_NHUELightState_Destroy( NHUELightState **this )
{
	// Free
	NFREE( (*this) );
}

/**
 * Is light on?
 *
 * @param this
 * 		This instance
 *
 * @return if the light is on
 */
NBOOL NDeviceHUE_Light_NHUELightState_IsOn( const NHUELightState *this )
{
	return this->m_isOn;
}

/**
 * Is reachable?
 *
 * @param this
 * 		This instance
 *
 * @return if the light is reachable
 */
NBOOL NDeviceHUE_Light_NHUELightState_IsReachable( const NHUELightState *this )
{
	return this->m_isReachable;
}

/**
 * Get the brightness
 *
 * @param this
 * 		This instance
 *
 * @return the brightness
 */
NU8 NDeviceHUE_Light_NHUELightState_GetBrightness( const NHUELightState *this )
{
	return this->m_brightness;
}

/**
 * Get the saturation
 *
 * @param this
 * 		This instance
 *
 * @return the saturation
 */
NU8 NDeviceHUE_Light_NHUELightState_GetSaturation( const NHUELightState *this )
{
	return this->m_saturation;
}

/**
 * Get the hue
 *
 * @param this
 * 		This instance
 *
 * @return the hue
 */
NU16 NDeviceHUE_Light_NHUELightState_GetHue( const NHUELightState *this )
{
	return this->m_hue;
}

/**
 * Get the xy color
 *
 * @param this
 * 		This instance
 *
 * @return the color at xy format
 */
NDoublePoint NDeviceHUE_Light_NHUELightState_GetXYColor( const NHUELightState *this )
{
	return this->m_xy;
}

/**
 * Get the color temperature
 *
 * @param this
 * 		This instance
 *
 * @return the color temperature
 */
NU32 NDeviceHUE_Light_NHUELightState_GetColorTemperature( const NHUELightState *this )
{
	return this->m_colorTemperature;
}

/**
 * Get light effect
 *
 * @param this
 * 		This instance
 *
 * @return the light effect
 */
NHUELightEffect NDeviceHUE_Light_NHUELightState_GetEffect( const NHUELightState *this )
{
	return this->m_effect;
}

/**
 * Get alert
 *
 * @param this
 * 		This instance
 *
 * @return the light alert
 */
NHUELightAlert NDeviceHUE_Light_NHUELightState_GetAlert( const NHUELightState *this )
{
	return this->m_alert;
}

/**
 * Get color mode
 *
 * @param this
 * 		This instance
 *
 * @return the light color mode
 */
NHUELightColorMode NDeviceHUE_Light_NHUELightState_GetColorMode( const NHUELightState *this )
{
	return this->m_colorMode;
}

/**
 * Display current light state
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Light_NHUELightState_Display( const NHUELightState *this )
{
	printf( "Color[ hs(%d;%d),\n\txy(%1.4f;%1.4f)\n\tct(%d)\n], IsReachable(%s), IsOn(%s),\nEffect(%s)\nColor mode(%s)\nAlert(%s)\n",
		this->m_hue,
		this->m_saturation,
		this->m_xy.x,
		this->m_xy.y,
		this->m_colorTemperature,
		this->m_isReachable ?
			"true"
			: "false",
		this->m_isOn ?
			"true"
			: "false",
		NDeviceHUE_Light_NHUELightEffect_GetName( this->m_effect ),
		NDeviceHUE_Light_NHUELightColorMode_GetName( this->m_colorMode ),
		NDeviceHUE_Light_NHUELightAlert_GetName( this->m_alert ) );
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELightState_BuildParserOutputListByService( const NHUELightState *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NDeviceHUELightServiceType serviceType )
{
	// Key
	char key[ 512 ];

	// Display separator?
	NBOOL isDisplaySeparator;

	// Color
	NCouleur color;

	// Display separator?
	isDisplaySeparator = (NBOOL)( strlen( keyRoot ) > 0 );

	switch( serviceType )
	{
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_ON:
			// Is on?
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( serviceType,
					NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"." ) );
			return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				this->m_isOn );
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_REACHABLE:
			// Is reachable?
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( serviceType,
					NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"." ) );
			return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				this->m_isReachable );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS:
			// Brightness
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( serviceType,
					NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"."NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_ROOT"." ) );
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_brightness );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION:
			// Saturation
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( serviceType,
					NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"."NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_ROOT"." ) );
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_saturation );
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE:
			// HUE
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( serviceType,
					NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"."NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_ROOT"." ) );
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_hue );
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
			// Build key
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( serviceType,
					NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"."NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_ROOT"." ) );

			// Convert hue/saturation/brightness to rgb
			NDeviceHUE_Light_ConvertHUESaturationBrightnessToRGB( this->m_hue,
				this->m_saturation,
				this->m_brightness,
				&color.r,
				&color.g,
				&color.b );

			// Add R/G/B value
			return (NBOOL)( NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
				key,
				color.r )
				&& NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
					key,
					color.g )
				&& NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
						key,
						color.b ) );

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT:
			// Effect
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT,
					NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"."NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ROOT"." ) );
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NDeviceHUE_Light_NHUELightEffect_GetName( this->m_effect ) );
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT:
			// Alert
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				isDisplaySeparator ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT,
					NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"."NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ROOT"." ) );
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NDeviceHUE_Light_NHUELightAlert_GetName( this->m_alert ) );

		default:
			break;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELightState_BuildParserOutputList( const NHUELightState *this,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Service type
	NDeviceHUELightServiceType serviceType = (NDeviceHUELightServiceType)0;

	// Key
	char key[ 512 ];

	// Root type
	NDeviceHUELightServiceType rootType;

	// Iterate
	for( ; serviceType < NDEVICE_HUE_LIGHT_SERVICE_TYPES; serviceType++ )
	{
		// Find root type
		switch( serviceType )
		{
			case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR:
			case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS:
			case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION:
			case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE:
			case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
				rootType = NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR;
				break;

			case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT:
			case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT:
				rootType = NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC;
				break;

			default:
				rootType = NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT;
				break;
		}

		// Create key
		snprintf( key,
			512,
			"%s",
			NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( rootType,
				NDEVICE_HUE_SERVICE_NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_ROOT"." ) );

		// Add element
		if( !NDeviceHUE_Light_NHUELightState_BuildParserOutputListByService( this,
			key,
			parserOutputList,
			serviceType ) )
			// Quit
			return NFALSE;
	}

	// OK
	return NTRUE;
}
