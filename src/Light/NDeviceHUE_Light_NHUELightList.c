#include "../../include/NDeviceHUE.h"

// ---------------------------------------
// struct NDeviceHUE::Light::NHUELightList
// ---------------------------------------

/**
 * Light receive callback
 *
 * @param packet
 * 		The received packet
 * @param client
 * 		The client
 *
 * @return if operation succedeed
 */
__PRIVATE __CALLBACK __WILLLOCK __WILLUNLOCK NBOOL NDeviceHUE_NHUELightList_CallbackReceiveLightList( const NPacket *packet,
	const NClient *client )
{
	// HTTP client
	const NClientHTTP *clientHTTP;

	// HUE Light list
	NHUELightList *this;

	// Secure data content
	char *securePacketData;

	// Parser result
	NParserOutputList *parserResult;

	// HTTP answer
	NReponseHTTP *httpAnswer;

	// Answer data
	char *answerData;

	// Light list
	NParserOutputList *lightParserOutputList;

	// Light element
	const NParserOutput *lightParserOutput;

	// A light
	NHUELight *light;

	// Iterator
	NU32 i;

	// Get client
	if( !( clientHTTP = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		// Get light list
		|| !( this = NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( clientHTTP ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Secure data
	if( !( securePacketData = NLib_Chaine_DupliquerSecurite( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// We're not waiting anymore
		this->m_isWaitingForLightList = NFALSE;

		// Quit
		return NFALSE;
	}

	// Build http answer
	if( !( httpAnswer = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( securePacketData ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( securePacketData );

		// We're not waiting anymore
		this->m_isWaitingForLightList = NFALSE;

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( securePacketData );

	// Get answer data
	if( !( answerData = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( httpAnswer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free answer
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &httpAnswer );

		// We're not waiting anymore
		this->m_isWaitingForLightList = NFALSE;

		// Quit
		return NFALSE;
	}

	// Free answer
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &httpAnswer );

	// Parse json
	if( !( parserResult = NJson_Engine_Parser_Parse( answerData,
		(NU32)strlen( answerData ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( answerData );

		// We're not waiting anymore
		this->m_isWaitingForLightList = NFALSE;

		// Quit
		return NFALSE;
	}

	// Free data
	NFREE( answerData );

	// Extract lights list
	if( !( lightParserOutputList = NParser_Output_NParserOutputList_GetAllOutput( parserResult,
		NDeviceHUE_Light_NHUELightProperty_GetProperty( NHUE_LIGHT_PROPERTY_MODEL_ID ),
		NFALSE,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Destroy result
		NParser_Output_NParserOutputList_Destroy( &parserResult );

		// We're not waiting anymore
		this->m_isWaitingForLightList = NFALSE;

		// Quit
		return NFALSE;
	}

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_light );

	// Clear light list
	while( NLib_Memoire_NListe_ObtenirNombre( this->m_light ) > 0 )
		// Destroy light
		NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_light,
			0 );

	// Build lights
	for( i = 0; i < NParser_Output_NParserOutputList_GetEntryCount( lightParserOutputList ); i++ )
	{
		// Get light info
		if( !( lightParserOutput = NParser_Output_NParserOutputList_GetEntry( lightParserOutputList,
			i ) ) )
			// Ignore failure
			continue;

		// Build light
		if( !( light = NDeviceHUE_Light_NHUELight_Build( NParser_Output_NParserOutput_GetKey( lightParserOutput ),
			NParser_Output_NParserOutput_GetValue( lightParserOutput ),
			this->m_authentication,
			parserResult ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_light );

			// Destroy lights list
			NParser_Output_NParserOutputList_Destroy( &lightParserOutputList );

			// Destroy result
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// We're not waiting anymore
			this->m_isWaitingForLightList = NFALSE;

			// Quit
			return NFALSE;
		}

		// Add light
		if( !NLib_Memoire_NListe_Ajouter( this->m_light,
			light ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_light );

			// Destroy lights list
			NParser_Output_NParserOutputList_Destroy( &lightParserOutputList );

			// Destroy result
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// We're not waiting anymore
			this->m_isWaitingForLightList = NFALSE;

			// Quit
			return NFALSE;
		}
	}

	// Clear protection
	NLib_Memoire_NListe_DesactiverProtection( this->m_light );

	// Destroy lights list
	NParser_Output_NParserOutputList_Destroy( &lightParserOutputList );

	// Destroy json parser output
	NParser_Output_NParserOutputList_Destroy( &parserResult );

	// We're not waiting anymore
	this->m_isWaitingForLightList = NFALSE;
	this->m_lastLightUpdateTimestamp = NLib_Temps_ObtenirTimestamp( );

	// OK
	return NTRUE;
}

/**
 * Thread for lights list obtention
 *
 * @param this
 * 		This instance
 *
 * @return if thread successfly did its job
 */
__PRIVATE __THREAD NBOOL NDeviceHUE_NHUELightList_ThreadLightObtention( NHUELightList *this )
{
	// Client HTTP
	NClientHTTP *clientHTTP;

	// HTTP request
	NRequeteHTTP *request;

	// Host IP
	const char *hostIP;

	// Buffer
	char buffer[ 2048 ];

	// Wait for the device to be ready
	while( !NDeviceHUE_NDeviceHUEAuthentication_IsReady( this->m_authentication )
	   	&& this->m_isWaitingForLightList
		&& ( this->m_autoUpdateDelay != 0 ?
			this->m_isAutoUpdateThreadRunning
			: NTRUE ) )
		// Wait
		NLib_Temps_Attendre( 16 );

	// Check readiness
	if( !NDeviceHUE_NDeviceHUEAuthentication_IsReady( this->m_authentication )
		|| !this->m_isWaitingForLightList )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Build client
	if( !( clientHTTP = NHTTP_Client_NClientHTTP_Construire2( NDeviceHUE_NDeviceHUEAuthentication_GetIP( this->m_authentication ),
		NDEVICE_COMMON_TYPE_HUE_LISTENING_PORT,
		this,
		NDeviceHUE_NHUELightList_CallbackReceiveLightList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Build request
	snprintf( buffer,
		2048,
		NDEVICEHUE_LIGHT_LIST_API_REQUEST,
		NDeviceHUE_NDeviceHUEAuthentication_GetAuthenticationID( this->m_authentication ) );

	// Build request
	if( ( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_GET,
		buffer ) ) != NULL )
	{
		// Add host information
		if( ( hostIP = NHTTP_Client_NClientHTTP_ObtenirIPServeur( clientHTTP ) ) != NULL )
			NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
				NMOT_CLEF_REQUETE_HTTP_KEYWORD_HOST,
				hostIP );

		// Send request
		if( !NHTTP_Client_NClientHTTP_EnvoyerRequete( clientHTTP,
			request,
			NDEVICEHUE_LIGHT_REQUEST_TIMEOUT ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

			// Destroy client
			NHTTP_Client_NClientHTTP_Detruire( &clientHTTP );

			// Quit
			return NFALSE;
		}
	}
	else
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy client
		NHTTP_Client_NClientHTTP_Detruire( &clientHTTP );

		// Quit
		return NFALSE;
	}

	// Wait for the light list
	while( this->m_isWaitingForLightList
		&& ( this->m_autoUpdateDelay != 0 ?
			  this->m_isAutoUpdateThreadRunning
			  : NTRUE ) )
		// Wait
		NLib_Temps_Attendre( 16 );

	// Destroy client
	NHTTP_Client_NClientHTTP_Detruire( &clientHTTP );

	// OK
	return NTRUE;
}

/**
 * Thread for auto light state update
 *
 * @param this
 * 		This instance
 *
 * @return if thread close well
 */
__PRIVATE __THREAD NBOOL NDeviceHUE_Light_NHUELightList_ThreadLightAutoUpdate( NHUELightList *this )
{
	// Update thread
	while( this->m_isAutoUpdateThreadRunning )
	{
		// Update lights
		NDeviceHUE_Light_NHUELightList_UpdateLightList( this );

		// Wait for delay
		NLib_Temps_Attendre( this->m_autoUpdateDelay );
	}

	// OK
	return NTRUE;
}

/**
 * Build the lights list
 *
 * @param authentication
 * 		The authentication informations
 * @param autoUpdateDelay
 * 		The auto light state update delay (ms) [Not updating if 0]
 *
 * @return the light list instance
 */
__ALLOC NHUELightList *NDeviceHUE_Light_NHUELightList_Build( const NDeviceHUEAuthentication *authentication,
	NU32 autoUpdateDelay )
{
	// Output
	__OUTPUT NHUELightList *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NHUELightList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create light list
	if( !( out->m_light = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NDeviceHUE_Light_NHUELight_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_authentication = authentication;
	out->m_autoUpdateDelay = autoUpdateDelay;

	// Update light list
	if( out->m_autoUpdateDelay > 0 )
	{
		// Start thread
		out->m_isAutoUpdateThreadRunning = NTRUE;
		if( !( out->m_lightUpdateThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NDeviceHUE_Light_NHUELightList_ThreadLightAutoUpdate,
			out,
			&out->m_isAutoUpdateThreadRunning ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Destroy light list
			NLib_Memoire_NListe_Detruire( &out->m_light );

			// Free
			NFREE( out );

			// Quit
			return NULL;
		}
	}
	else
		if( !NDeviceHUE_Light_NHUELightList_UpdateLightList( out ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Destroy light list
			NLib_Memoire_NListe_Detruire( &out->m_light );

			// Free
			NFREE( out );

			// Quit
			return NULL;
		}

	// OK
	return out;
}

/**
 * Destroy light list
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Light_NHUELightList_Destroy( NHUELightList **this )
{
	// Destroy light update thread
	if( (*this)->m_lightUpdateThread != NULL )
		NLib_Thread_NThread_Detruire( &(*this)->m_lightUpdateThread );

	// Destroy light obtention thread
	if( (*this)->m_lightObtentionThread != NULL )
		NLib_Thread_NThread_Detruire( &(*this)->m_lightObtentionThread );

	// Destroy light list
	NLib_Memoire_NListe_Detruire( &(*this)->m_light );

	// Free
	NFREE( (*this) );
}

/**
 * Update light list
 *
 * @param this
 * 		This instance
 *
 * @return if update successfully started
 */
NBOOL NDeviceHUE_Light_NHUELightList_UpdateLightList( NHUELightList *this )
{
	// If we already have an obtention thread
	if( this->m_lightObtentionThread != NULL )
		// Destroy thread
		NLib_Thread_NThread_Detruire( &this->m_lightObtentionThread );

	// Start light obtention thread
	this->m_isWaitingForLightList = NTRUE;
	if( !( this->m_lightObtentionThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NDeviceHUE_NHUELightList_ThreadLightObtention,
		this,
		&this->m_isWaitingForLightList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Activate protection on list
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NDeviceHUE_Light_NHUELightList_ActivateProtection( NHUELightList *this )
{
	NLib_Memoire_NListe_ActiverProtection( this->m_light );
}

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NDeviceHUE_Light_NHUELightList_DeactivateProtection( NHUELightList *this )
{
	NLib_Memoire_NListe_DesactiverProtection( this->m_light );
}

/**
 * Find a light index by id
 *
 * @param this
 * 		This instance
 * @param id
 * 		The light id
 *
 * @return the light index or NERREUR if not found
 */
__PRIVATE __MUSTBEPROTECTED NU32 NDeviceHUE_Light_NHUELightList_FindLightIndexByID( NHUELightList *this,
	NU32 id )
{
	// Iterator
	__OUTPUT NU32 i = 0;

	// Light
	const NHUELight *light;

	// Look for light
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_light ); i++ )
	{
		// Get light
		if( !( light = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
			i ) ) )
			// Ignore failure
			continue;

		// Check light id
		if( NDeviceHUE_Light_NHUELight_GetID( light ) == id )
			// Found it!
			return i;
	}

	// Not found
	return NERREUR;
}

/**
 * Find a light index by name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The light name
 *
 * @return the light index or NERREUR if not found
 */
__PRIVATE __MUSTBEPROTECTED NU32 NDeviceHUE_Light_NHUELightList_FindLightIndexByName( NHUELightList *this,
	const char *name )
{
	// Iterator
	__OUTPUT NU32 i = 0;

	// Light
	const NHUELight *light;

	// Look for light
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_light ); i++ )
	{
		// Get light
		if( !( light = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
			i ) ) )
			// Ignore failure
			continue;

		// Check light id
		if( NLib_Chaine_Comparer( NDeviceHUE_Light_NHUELight_GetName( light ),
			name,
			NTRUE,
			0 ) )
			// Found it!
			return i;
	}

	// Not found
	return NERREUR;
}

/**
 * Find a light by id
 *
 * @param this
 * 		This instance
 * @param id
 * 		The light id
 *
 * @return the light
 */
__MUSTBEPROTECTED NHUELight *NDeviceHUE_Light_NHUELightList_FindLightByID( NHUELightList *this,
	NU32 id )
{
	// Index
	NU32 index;

	// Get light index
	if( ( index = NDeviceHUE_Light_NHUELightList_FindLightIndexByID( this,
		id ) ) == NERREUR )
		return NULL;

	// OK
	return (NHUELight*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
		index );
}

/**
 * Find a light by name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The light name
 *
 * @return the light
 */
__MUSTBEPROTECTED NHUELight *NDeviceHUE_Light_NHUELightList_FindLightByName( NHUELightList *this,
	const char *name )
{
	// Index
	NU32 index;

	// Get light index
	if( ( index = NDeviceHUE_Light_NHUELightList_FindLightIndexByName( this,
		name ) ) == NERREUR )
		return NULL;

	// OK
	return (NHUELight*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
		index );
}

/**
 * Get light count
 *
 * @param this
 * 		This instance
 *
 * @return the light count
 */
__MUSTBEPROTECTED NU32 NDeviceHUE_Light_NHUELightList_GetLightCount( const NHUELightList *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_light );
}

/**
 * Get light by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The light index
 *
 * @return the light
 */
__MUSTBEPROTECTED const NHUELight *NDeviceHUE_Light_NHUELightList_GetLightByIndex( const NHUELightList *this,
	NU32 index )
{
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
		index );
}

// Way to get light by id or by index
#define GET_LIGHT( ) \
	if( !( light = ( isLightIdentifierId ? \
		NDeviceHUE_Light_NHUELightList_FindLightByID( this, \
			lightIdentifier ) \
		: (NHUELight*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light, \
			lightIdentifier ) ) ) ) \
	{ \
		/* Notify */ \
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR ); \
 \
		/* Quit */ \
		return NFALSE; \
	}

/**
 * Activate a light
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ActivateLight( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ActivateLight( light );
}

/**
 * Deactivate a light
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_DeactivateLight( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_DeactivateLight( light );
}

/**
 * Short blink a light (will shutdown the light after blink)
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ShortBlinkLight( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ShortBlinkLight( light );
}

/**
 * Blink a light
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_BlinkLight( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_BlinkLight( light );
}

/**
 * Long blink a light (for 15 seconds, with a period of 1 second)
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if the operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_LongBlinkLight( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_LongBlinkLight( light );
}

/**
 * Activate color loop
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ActivateColorLoop( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ActivateColorLoop( light );
}

/**
 * Deactivate color loop
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_DeactivateColorLoop( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_DeactivateColorLoop( light );
}

/**
 * Change light intensity
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 * @param brightness
 * 		The light intensity (1-254)
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeIntensity( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU8 brightness )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ChangeIntensity( light,
		brightness );
}
/**
 * Change color and intensity
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param hue
 * 		The hue value (0 to 65535)
 * @param saturation
 * 		The saturation value (0 to 255)
 * @param brightness
 * 		The brightness (0 to 255)
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColorAndIntensity( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU16 hue,
	NU8 saturation,
	NU8 brightness )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ChangeColorAndIntensity( light,
		hue,
		saturation,
		brightness );
}

/**
 * Change light color with hue/saturation combination
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 * @param hue
 * 		The hue value (0 to 65535)
 * @param saturation
 * 		The saturation value
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU16 hue,
	NU8 saturation )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ChangeColor( light,
		hue,
		saturation );
}

/**
 * Change light color with xy combination
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 * @param x
 * 		The x color coordinate
 * @param y
 * 		The y color coordinate
 *
 * @return if operation succedeed
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor2( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	double x,
	double y )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ChangeColor2( light,
		x,
		y );
}

/**
 * Change light color with hue/saturation combination
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 * @param colorTemperature
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor3( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU16 colorTemperature )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ChangeColor3( light,
		colorTemperature );
}

/**
 * Change color with rgb value
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param r
 * 		The red value
 * @param g
 * 		The green value
 * @param b
 * 		The blue value
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor4( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NU8 r,
	NU8 g,
	NU8 b )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ChangeColor4( light,
		r,
		g,
		b );
}

/**
 * Change color with rgb value
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param color
 * 		The rgb color
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor5( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	NCouleur color )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ChangeColor5( light,
		color );
}

/**
 * Change color with color name
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierIdentifierId
 * 		Is it an id ? If not this is an index
 * @param colorName
 * 		The color name
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeColor6( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	const char *colorName )
{
	// Color values
	const NU8 *colorValue;

	// Get color values
	if( !( colorValue = NDeviceHUE_Light_NHUELightColor_FindValueFromName( colorName ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Change color
	return NDeviceHUE_Light_NHUELightList_ChangeColor4( this,
		lightIdentifier,
		isLightIdentifierId,
		colorValue[ 0 ],
		colorValue[ 1 ],
		colorValue[ 2 ] );
}

/**
 * Set the light name
 *
 * @param this
 * 		This instance
 * @param lightIdentifier
 * 		The light id or index
 * @param isLightIdentifierId
 * 		Is it an id ? If not this is an index
 * @param name
 * 		The new light name
 *
 * @return if operation succedeed
 */
__MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ChangeName( NHUELightList *this,
	NU32 lightIdentifier,
	NBOOL isLightIdentifierId,
	const char *name )
{
	// Light
	NHUELight *light;

	// Get light
	GET_LIGHT( );

	// OK
	return NDeviceHUE_Light_NHUELight_ChangeName( light,
		name );
}

#undef GET_LIGHT

/**
 * Build parser output state (private)
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 * @param lightIndex
 * 		The light index
 * @param isAddRootKey
 * 		Do we add root key?
 *
 * @return if operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_BuildParserOutputListInternal( const NHUELightList *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NDeviceHUELightListServiceType serviceType,
	NU32 lightIndex,
	NBOOL isAddRootKey )
{
	// Light
	const NHUELight *light;

	// Key
	char key[ 512 ];

	switch( serviceType )
	{
		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LIGHT_DETAIL:
			if( ( light = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
				lightIndex ) ) != NULL )
			{
				// Prepare key
				snprintf( key,
					512,
					"%s%s%s%s%d",
					keyRoot,
					strlen( keyRoot ) > 0 ?
						"."
						: "",
					NDeviceHUE_Service_NDeviceHUELightListServiceType_GetServiceKey( NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT,
						isAddRootKey ),
					strlen( NDeviceHUE_Service_NDeviceHUELightListServiceType_GetServiceKey( NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT,
						isAddRootKey ) ) > 0 ?
						"."
						: "",
					lightIndex );

				// Add light informations
				NDeviceHUE_Light_NHUELight_BuildParserOutputList( light,
					key,
					parserOutputList );
			}
			break;
		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_COUNT:
			// Prepare key
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				strlen( keyRoot ) > 0 ?
					"."
					: "",
			NDeviceHUE_Service_NDeviceHUELightListServiceType_GetServiceKey( NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_COUNT,
				isAddRootKey ) );

			// Add value
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				NLib_Memoire_NListe_ObtenirNombre( this->m_light ) );
			break;
		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LAST_UPDATE_TIMESTAMP:
			// Prepare key
			snprintf( key,
				512,
				"%s%s%s",
				keyRoot,
				strlen( keyRoot ) > 0 ?
					"."
					: "",
				NDeviceHUE_Service_NDeviceHUELightListServiceType_GetServiceKey( NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LAST_UPDATE_TIMESTAMP,
					isAddRootKey ) );

			// Add value
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				(NU32)this->m_lastLightUpdateTimestamp );
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output state
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param isAddRootKey
 * 		Do we add root key?
 *
 * @return if operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceHUE_Light_NHUELightList_BuildParserOutputList( const NHUELightList *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList,
	NBOOL isAddRootKey )
{
	// Service type
	NDeviceHUELightListServiceType serviceType = (NDeviceHUELightListServiceType)0;

	// Iterator
	NU32 i;

	// Lock mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_light );

	// Iterate services
	for( ; serviceType < NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPES; serviceType++ )
	{
		switch( serviceType )
		{
			default:
				NDeviceHUE_Light_NHUELightList_BuildParserOutputListInternal( this,
					keyRoot,
					parserOutputList,
					serviceType,
					0,
					isAddRootKey );
				break;

			case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LIGHT_DETAIL:
				for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_light ); i++ )
					NDeviceHUE_Light_NHUELightList_BuildParserOutputListInternal( this,
						keyRoot,
						parserOutputList,
						serviceType,
						i,
						isAddRootKey );
				break;
		}
	}

	// Unlock mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_light );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param element
 * 		The element to GET
 * @param cursor
 * 		Cursor in element
 *
 * @return if operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceHUE_Light_NHUELightList_ProcessRESTGETRequest( const NHUELightList *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *element,
	NU32 *cursor )
{
	// Service type
	NDeviceHUELightListServiceType serviceType;

	// Return code
	__OUTPUT NBOOL returnCode;

	// Light index
	NU32 lightIndex;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_light );

	// Find service type
	switch( ( serviceType = NDeviceHUE_Service_NDeviceHUELightListServiceType_FindService( element,
		cursor,
		this,
		&lightIndex ) ) )
	{
		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT:
			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_light );

			// Process standard request
			return NDeviceHUE_Light_NHUELightList_BuildParserOutputList( this,
				"",
				parserOutputList,
				NFALSE );

		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LAST_UPDATE_TIMESTAMP:
		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_COUNT:
			returnCode = NDeviceHUE_Light_NHUELightList_BuildParserOutputListInternal(this,
				"",
				parserOutputList,
				serviceType,
				0,
				NFALSE );
			break;

		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LIGHT_DETAIL:
			returnCode = NDeviceHUE_Light_NHUELight_ProcessRESTGetRequest( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
					lightIndex ),
				parserOutputList,
				element,
				cursor );
			break;

		default:
			returnCode = NFALSE;
			break;
	}

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_light );

	// OK?
	return returnCode;
}

/**
 * Detect light modification list (private)
 *
 * @param this
 * 		This instance
 * @param clientDataJson
 * 		The client data
 *
 * @return the light modification list
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NDeviceHUE_Light_NHUELightList_ApplyLightModification( NHUELightList *this,
	const NParserOutputList *clientDataJson )
{
	// Iterators
	NU32 i,
		j;

	// Entry
	const NParserOutput *entry;

	// Cursor
	NU32 cursor;

	// Output
	NHUELightModification *modification;

	// Light
	NHUELight *light;

	// Is modify all lights with same value?
	NBOOL isModifyAllLightSameValue;

	// Is must be root?
	NBOOL isMustBeRoot = NFALSE;

	// Service type
	NDeviceHUELightServiceType serviceType;

	// Light index
	NU32 lightIndex;

	// RGB color processed?
	NBOOL isRGBProcessed = NFALSE;

	// Check parameter
	if( NParser_Output_NParserOutputList_GetEntryCount( clientDataJson ) <= 0
		|| ( entry = NParser_Output_NParserOutputList_GetEntry( clientDataJson,
			0 ) ) == NULL )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Determine request type
	isModifyAllLightSameValue = (NBOOL)( !NLib_Caractere_EstUnChiffre( NParser_Output_NParserOutput_GetKey( entry )[ 0 ] ) );

	// All lights with same values
	if( isModifyAllLightSameValue )
	{
		// Build modification
		if( !( modification = NDeviceHUE_Service_PUT_NHUELightModification_Build( NULL ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quit
			return NFALSE;
		}

		// Iterate keys
		for( i = 0; i < NParser_Output_NParserOutputList_GetEntryCount( clientDataJson ); i++ )
			// Get entry
			if( ( entry = NParser_Output_NParserOutputList_GetEntry( clientDataJson,
				i ) ) != NULL )
			{
				// Find service
				cursor = 0;
				if( ( serviceType = NDeviceHUE_Service_NDeviceHUELightServiceType_FindService( NParser_Output_NParserOutput_GetKey( entry ),
					'.',
					cursor ) ) >= NDEVICE_HUE_LIGHT_SERVICE_TYPES )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_SYNTAX );

					// Quit
					return NFALSE;
				}

				// Add property
				switch( serviceType )
				{
					case NDEVICE_HUE_LIGHT_SERVICE_TYPE_NAME:
						isMustBeRoot = NTRUE;
						break;
					case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
						// Check if already processed
						if( isRGBProcessed )
							break;

						// Process
						NDeviceHUE_Light_ProcessRGBPropertyModificationAdd( clientDataJson,
							modification,
							NParser_Output_NParserOutput_GetKey( entry ) );

						// Now done
						isRGBProcessed = NTRUE;
						break;
					default:
						if( !NDeviceHUE_Service_PUT_NHUELightModification_AddProperty( modification,
							serviceType,
							entry ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Destroy
							NDeviceHUE_Service_PUT_NHUELightModification_Destroy( &modification );

							// Quit
							return NFALSE;
						}
						break;
				}
			}

		// Apply modification
		for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_light ); i++ )
			// Get light
			if( ( light = (NHUELight*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
				i ) ) != NULL )
			{
				// Set light
				NDeviceHUE_Service_PUT_NHUELightModification_SetLight( modification,
					light );

				// Send
				NDeviceHUE_Service_PUT_NHUELightModification_SendModification( modification,
					(NBOOL)( !isMustBeRoot ) );
			}

		// Destroy modification
		NDeviceHUE_Service_PUT_NHUELightModification_Destroy( &modification );
	}
	else
	{
		// Iterate lights
		for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_light ); i++ )
			// Get light
			if( ( light = (NHUELight*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
				i ) ) != NULL )
			{
				// Build modification
				if( !( modification = NDeviceHUE_Service_PUT_NHUELightModification_Build( light ) ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Quit
					return NFALSE;
				}

				// Iterate keys
				for( j = 0; j < NParser_Output_NParserOutputList_GetEntryCount( clientDataJson ); j++ )
					// Get entry
					if( ( entry = NParser_Output_NParserOutputList_GetEntry( clientDataJson,
						j ) ) != NULL )
					{
						// Cursor to zero
						cursor = 0;

						// Read index
						if( ( lightIndex = NLib_Chaine_LireNombreNonSigne( NParser_Output_NParserOutput_GetKey( entry ),
							&cursor,
							NFALSE ) ) == NERREUR )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Destroy
							NDeviceHUE_Service_PUT_NHUELightModification_Destroy( &modification );

							// Quit
							return NFALSE;
						}

						// Is it what we want?
						if( lightIndex != i )
							// Ignore
							continue;

						// Place to correct position
						cursor = 0;
						if( !NLib_Chaine_PlacerAuCaractere( NParser_Output_NParserOutput_GetKey( entry ),
							'.',
							&cursor ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Destroy
							NDeviceHUE_Service_PUT_NHUELightModification_Destroy( &modification );

							// Quit
							return NFALSE;
						}

						// Find service type
						if( ( serviceType = NDeviceHUE_Service_NDeviceHUELightServiceType_FindService( NParser_Output_NParserOutput_GetKey( entry ),
							'.',
							cursor ) ) >= NDEVICE_HUE_LIGHT_SERVICE_TYPES )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Destroy
							NDeviceHUE_Service_PUT_NHUELightModification_Destroy( &modification );

							// Quit
							return NFALSE;
						}

						// Check service type
						switch( serviceType )
						{
							case NDEVICE_HUE_LIGHT_SERVICE_TYPE_NAME:
								isMustBeRoot = NTRUE;
								break;
							case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_RGB:
								// Check if already processed
								if( isRGBProcessed )
									break;

								// Process
								NDeviceHUE_Light_ProcessRGBPropertyModificationAdd( clientDataJson,
									modification,
									NParser_Output_NParserOutput_GetKey( entry ) );

								// Done
								isRGBProcessed = NTRUE;
								break;
							default:
								// Add property
								if( !NDeviceHUE_Service_PUT_NHUELightModification_AddProperty( modification,
									serviceType,
									entry ) )
								{
									// Notify
									NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

									// Destroy
									NDeviceHUE_Service_PUT_NHUELightModification_Destroy( &modification );

									// Quit
									return NFALSE;
								}
								break;
						}
					}

				// Check if there are modification(s) to apply
				if( NDeviceHUE_Service_PUT_NHUELightModification_IsModification( modification ) )
					// Send modifications
					NDeviceHUE_Service_PUT_NHUELightModification_SendModification( modification,
						(NBOOL)( !isMustBeRoot ) );

				// Destroy
				NDeviceHUE_Service_PUT_NHUELightModification_Destroy( &modification );
			}

	}

	// OK
	return NTRUE;
}

/**
 * Process REST PUT request
 *
 * @param this
 * 		This instance
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param clientDataJson
 * 		The client parsed data
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Light_NHUELightList_ProcessRESTPUTRequest( const NHUELightList *this,
	const char *element,
	NU32 *cursor,
	const NParserOutputList *clientDataJson )
{
	// Return code
	__OUTPUT NBOOL returnCode = NFALSE;

	// Light index
	NU32 lightIndex;

	// Light
	NHUELight *light;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_light );

	// Find service type
	switch( NDeviceHUE_Service_NDeviceHUELightListServiceType_FindService( element,
		cursor,
		this,
		&lightIndex ) )
	{
		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_ROOT:
			// Process standard request
			returnCode = NDeviceHUE_Light_NHUELightList_ApplyLightModification( (NHUELightList*)this,
				clientDataJson );
			break;

		case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LIGHT_DETAIL:
			// Get light
			if( ( light = (NHUELight*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_light,
				lightIndex ) ) != NULL )
				// Process request
				returnCode = NDeviceHUE_Light_NHUELight_ProcessRESTPUTRequest( light,
					element,
					*cursor,
					clientDataJson );
			break;

		default:
			break;
	}

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_light );

	// OK?
	return returnCode;
}

