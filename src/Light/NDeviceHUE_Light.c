#include "../../include/NDeviceHUE.h"

// ---------------------------
// namespace NDeviceHUE::Light
// ---------------------------

/**
 * Convert rgb to hue/saturation/brightness
 *
 * @param color
 * 		The rgb color
 * @param hue
 * 		The output hue
 * @param saturation
 * 		The output saturation
 * @param brightness
 * 		The output brightness
 */
void NDeviceHUE_Light_ConvertRGBToHUESaturationBrightness( NU8 r,
	NU8 g,
	NU8 b,
	__OUTPUT NU16 *hue,
	__OUTPUT NU8 *saturation,
	__OUTPUT NU8 *brightness )
{
	// Value between 0 and 1
	double correctedR,
		correctedG,
		correctedB;

	// Maximum
	char maximum;
	double maximumValue;

	// Minimum
	double minimumValue;


	// Luminance value
	double luminance;

	// Convert
	correctedR = (double)r / 255.0;
	correctedG = (double)g / 255.0;
	correctedB = (double)b / 255.0;

	// Find maximum
	if( correctedR > correctedG )
	{
		if( correctedR > correctedB )
		{
			maximum = 'R';
			maximumValue = correctedR;
		}
		else
		{
			maximum = 'B';
			maximumValue = correctedB;
		}
	}
	else
	{
		if( correctedG > correctedB )
		{
			maximum = 'G';
			maximumValue = correctedG;
		}
		else
		{
			maximum = 'B';
			maximumValue = correctedB;
		}
	}

	// Find minimum
	if( correctedR < correctedG )
	{
		if( correctedR < correctedB )
			minimumValue = correctedR;
		else
			minimumValue = correctedB;
	}
	else
	{
		if( correctedG < correctedB )
			minimumValue = correctedG;
		else
			minimumValue = correctedB;
	}

	// Calculate luminance
	luminance = ( minimumValue + maximumValue ) / 2.0;

	// Calculate saturation
	if( luminance < 0.5 )
		*saturation = (NU8)( ( ( maximumValue -minimumValue ) / ( maximumValue + minimumValue ) ) * 255.0 );
	else
		*saturation = (NU8)( ( ( maximumValue - minimumValue ) / ( 2.0 - maximumValue - minimumValue ) ) * 255.0 );

	// Analyze maximum
	switch( maximum )
	{
		case 'R':
			*hue = (NU16)( ( ( correctedG - correctedB ) / ( maximumValue - minimumValue ) ) * 60.0 );
			break;
		case 'G':
			*hue = (NU16)( ( 2.0 + ( correctedB - correctedR ) / ( maximumValue - minimumValue ) ) * 60.0 );
			break;
		case 'B':
			*hue = (NU16)( ( 4.0 + ( correctedR - correctedG ) / ( maximumValue - minimumValue ) ) * 60.0 );
			break;

		default:
			break;
	}

	// Adapt hue value
	( *hue ) *= NDEVICEHUE_HUE_CONVERSION_FACTOR;

	// Correct brightness
	*brightness = (NU8)( luminance * 255.0 );
}

/**
 * Convert hue to rgb
 *
 * @param p
 * @param q
 * @param hue
 * 		The hue to convert
 *
 * @return p
 */
__PRIVATE double NDeviceHUE_Light_InternalConvertionHUEToRGB( double p,
	double q,
	double hue )
{
	if( hue < 0 )
		hue += 1;
	if( hue > 1 )
		hue -= 1;
	if( hue * 6.0 < 1.0 )
		return p + ( q - p ) * 6.0 * hue;
	if( hue * 2.0 < 1.0 )
		return q;
	if( hue * 3.0 < 2.0 )
		return p + ( q - p ) * ( 2.0 / 3.0 - hue ) * 6;

	return p;
}

/**
 * Convert HSV to HSL
 *
 * @param hue
 * 		The source hue
 * @param saturation
 * 		The source saturation
 * @param brightness
 * 		The source brightness
 * @param convertedHUE
 * 		The converted HUE
 * @param convertedSaturation
 * 		The converted saturation
 * @param convertedBrightness
 * 		The converted brightness
 */
__PRIVATE void NDeviceHUE_Light_ConvertHSVtoHSL( NU16 hue,
	NU8 saturation,
	NU8 brightness,
	__OUTPUT NU16 *convertedHUE,
	__OUTPUT NU8 *convertedSaturation,
	__OUTPUT NU8 *convertedBrightness )
{
	double _h = ( (double)hue / 65535.0 ),
		_s = ( (double)saturation / 255.0 ) * ( (double)brightness / 255.0 ),
		_l = ( 2 - ( (double)saturation / 255.0 ) ) * ( (double)brightness / 255.0 );
	_s /= (_l <= 1) ? _l : 2 - _l;
	_l /= 2;

	*convertedHUE = (NU16)( _h * 65535.0 );
	*convertedBrightness = (NU8)( _l * 255.0 );
	*convertedSaturation = (NU8)( _s * 255.0 );
}

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 * (https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion)
 *
 * @param   {number}  h       The hue
 * @param   {number}  s       The saturation
 * @param   {number}  l       The lightness
 * @return  {Array}           The RGB representation
 */
void NDeviceHUE_Light_ConvertHUESaturationBrightnessToRGB( NU16 hue,
	NU8 saturation,
	NU8 brightness,
	__OUTPUT NU8 *r,
	__OUTPUT NU8 *g,
	__OUTPUT NU8 *b )
{
	// Corrected values
	double correctedHUE;
	double correctedSaturation;
	double correctedBrightness;

	// Temp variable
	double q,
		p;

	NDeviceHUE_Light_ConvertHSVtoHSL( hue,
		saturation,
		brightness,
		&hue,
		&saturation,
		&brightness );

	// Correct values
	correctedHUE = (double)hue / 65535.0;
	correctedSaturation = (double)saturation / 255.0;
	correctedBrightness = (double)brightness / 255.0;

	// Calculate p
	if( correctedBrightness <= 0.5 )
		p = correctedBrightness * ( correctedSaturation+1 );
	else
		p = correctedBrightness + correctedSaturation - correctedBrightness * correctedSaturation;

	// Calculate q
	q = correctedBrightness * 2 - p;

	// Convert
	*r = (NU8)( NDeviceHUE_Light_InternalConvertionHUEToRGB( q, p, correctedHUE + 1.0 / 3.0 ) * 255.0 );
	*g = (NU8)( NDeviceHUE_Light_InternalConvertionHUEToRGB( q, p, correctedHUE ) * 255.0 );
	*b = (NU8)( NDeviceHUE_Light_InternalConvertionHUEToRGB( q, p, correctedHUE - 1.0 / 3.0 ) * 255.0 );
}

/**
 * Process RGB property to add correct attributes
 *
 * @param clientJsonData
 * 		The client data
 * @param lightModification
 * 		Output light modification
 * @param keyName
 * 		The key name
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Light_ProcessRGBPropertyModificationAdd( const NParserOutputList *clientJsonData,
	__OUTPUT NHUELightModification *lightModification,
	const char *keyName )
{
	// Parser entry list
	NParserOutputList *entryList;

	// HUE/Saturation/Brightness
	NU16 hue;
	NU8 saturation,
		brightness;

	// Parser entry color rgb
	const NParserOutput *parserOutputRGB[ 3 ];

	// Iterator
	NU32 i;

	// List
	if( ( entryList = NParser_Output_NParserOutputList_GetAllOutput( clientJsonData,
		keyName,
		NTRUE,
		NTRUE ) ) != NULL )
	{
		// Check count
		if( NParser_Output_NParserOutputList_GetEntryCount( entryList ) == 3 )
		{
			// Copy
			for( i = 0; i < 3; i++ )
				if( NParser_Output_NParserOutput_GetType( parserOutputRGB[ i ] = NParser_Output_NParserOutputList_GetEntry( entryList,
					i ) ) != NPARSER_OUTPUT_TYPE_INTEGER )
				{
					// Destroy
					NParser_Output_NParserOutputList_Destroy( &entryList );

					// Quit
					return NFALSE;
				}

			// Convert
			NDeviceHUE_Light_ConvertRGBToHUESaturationBrightness( (NU8)*(NU32*)NParser_Output_NParserOutput_GetValue( parserOutputRGB[ 0 ] ),
				(NU8)*(NU32*)NParser_Output_NParserOutput_GetValue( parserOutputRGB[ 1 ] ),
				(NU8)*(NU32*)NParser_Output_NParserOutput_GetValue( parserOutputRGB[ 2 ] ),
				&hue,
				&saturation,
				&brightness );

			// Add properties
			NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyInteger( lightModification,
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetHUEMapping( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE ),
				hue );
			NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyInteger( lightModification,
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetHUEMapping( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION ),
				saturation );
			NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyInteger( lightModification,
				NDeviceHUE_Service_NDeviceHUELightServiceType_GetHUEMapping( NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS ),
				brightness );
		}

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &entryList );
	}

	// OK
	return NTRUE;
}
