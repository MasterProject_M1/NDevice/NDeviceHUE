#include "../include/NDeviceHUE.h"

// --------------------
// namespace NDeviceHUE
// --------------------

#ifdef NDEVICE_HUE_TEST_PROJECT
/**
 * Test control
 * These commands have to be typed in the console once program is started
 *
 * Commands example:
 *
 * - Set different random color for each light:
 * rc
 *
 * - Set same random color for all lights:
 * ac
 *
 * - Set specific color (HS:5000;SAT:255) on all lights:
 * sc 5000 255
 *
 * - Blink all lights:
 * sb
 *
 * - Quit
 * sq
 *
 * - Light on all lights
 * so
 *
 * - Light off all lights
 * sf
 *
 * - Set brightness to 255 on all lights
 * si 255
 *
 * - Random brightness different on all lights
 * ri
 *
 * - Random brightness same on all lights
 * ai
 */

/**
 * Self dedicated error callback
 *
 * @param error
 * 		The error
 */
void CallbackErreur( const NErreur *error )
{
	// Afficher message?
	NBOOL estAfficher = NTRUE;

	// On n'affichera pas les avertissements
#ifndef NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS
	if( NLib_Erreur_NErreur_ObtenirNiveau( error ) <= NNIVEAU_ERREUR_AVERTISSEMENT )
		estAfficher = NFALSE;
#endif // NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS

	// Doit afficher?
	switch( NLib_Erreur_NErreur_ObtenirCode( error ) )
	{
		case NERREUR_SOCKET_RECV:
		case NERREUR_SOCKET_SEND:
			estAfficher = NFALSE;
			break;

		case NERREUR_USER:
			estAfficher = NTRUE;
			break;

		default:
			break;
	}

	// Afficher l'erreur
	if( estAfficher )
		switch( NLib_Erreur_NErreur_ObtenirNiveau( error ) )
		{
			case NNIVEAU_ERREUR_ERREUR:
				// Erreur
				printf( "[%s]: %s( )::(%s)\n",
					NLib_Erreur_NNiveauErreur_Traduire( NLib_Erreur_NErreur_ObtenirNiveau( error ) ),
					NLib_Erreur_NErreur_ObtenirMessage( error ),
					NLib_Erreur_NCodeErreur_Traduire( NLib_Erreur_NErreur_ObtenirCode( error ) ) );

				// Origine
				printf( "\"%s\", ligne %d, code %d.\n\n",
					NLib_Erreur_NErreur_ObtenirFichier( error ),
					NLib_Erreur_NErreur_ObtenirLigne( error ),
					NLib_Erreur_NErreur_ObtenirCodeUtilisateur( error ) );
				break;

			case NNIVEAU_ERREUR_AVERTISSEMENT:
				puts( NLib_Erreur_NErreur_ObtenirMessage( error ) );
				break;

			default:
				break;
		}
}

/**
 * The entry point
 *
 * @param argc
 * 		The argument count
 * @param argv
 * 		The argument vector
 *
 * @return EXIT_SUCCESS if ok
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Device
	NDeviceHUE *hue;

	// Light list
	NHUELightList *lightList;

	// Light
	NHUELight *light;

	// Ignore display?
	NBOOL isIgnoreDisplay = NFALSE;

	// Iterator
	NU32 i;

	// Character
	char cM,
		cA;

	// Values
	NU32 v1 = 0,
		v2 = 0,
		v3 = 0;

	// Initialize NLib
	NLib_Initialiser( CallbackErreur );

	// Create device
	if( !( hue = NDeviceHUE_NDeviceHUE_Build( "0017-88FF-FEA1-1308",
		"192.168.1.29",
		0,
		10000 ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return EXIT_FAILURE;
	}

	// Wait for authentication
	while( !NDeviceHUE_NDeviceHUE_IsReady( hue ) )
		NLib_Temps_Attendre( 16 );

	// Display the obtained id
	printf( "ID = %s\n",
		NDeviceHUE_NDeviceHUE_GetAuthenticationID( hue ) );

	// Wait a bit
	NLib_Temps_Attendre( 1000 );

	// Update devices
	NDeviceHUE_NDeviceHUE_UpdateLightList( hue );

	// Get light list
	lightList = NDeviceHUE_NDeviceHUE_GetLightList( hue );

	// Test control
	do
	{
		// List commands
		if( !isIgnoreDisplay )
		{
			puts( "Choose command (combine lines)..." );

			puts( "r: random, s: set, a: all random with same value" );
			puts( "b: Blink, l: Long blink, s: Short blink, o: Light on, f: Light off, c: Color (hs), i: Brightness, r: Color (rgb), q: Quit\n" );
		}

		// Wait for character
		if( ( cM = (char)getchar( ) ) == '\n'
			|| ( cA = (char)getchar( ) ) == '\n' )
		{
			isIgnoreDisplay = NTRUE;
			continue;
		}
		else
			isIgnoreDisplay = NFALSE;

		// Set values according to method
		if( cM == 'a' )
		{
			v1 = (NU32)( rand( ) % 0xFFFF );
			v2 = (NU32)( rand( ) % 0xFFFF );
			v3 = (NU32)( rand( ) % 0xFFFF );
		}
		else if( cM == 's' )
		{
			switch( cA )
			{
				case 'c':
					// Set value
					scanf( "%d",
							&v1 );
					scanf( "%d",
						&v2 );
					break;
				case 'i':
					// Set value
					scanf( "%d",
						&v1 );
					break;
				case 'r':
					// Set value
					scanf( "%d",
						&v1 );
					scanf( "%d",
						&v2 );
					scanf( "%d",
						&v3 );
					break;

				default:
					break;
			}
		}

		// Display
		puts( "Current light state:\n" );

		// Lock light list
		NDeviceHUE_Light_NHUELightList_ActivateProtection( hue->m_light );

		// Control lights
		for( i = 0; i < NDeviceHUE_Light_NHUELightList_GetLightCount( hue->m_light ); i++ )
		{
			// Set value
			if( cM == 'r' )
			{
				v1 = (NU32)( rand( ) % 0xFFFF );
				v2 = (NU32)( rand( ) % 0xFFFF );
				v3 = (NU32)( rand( ) % 0xFFFF );
			}

			// Analyse action
			switch( cA )
			{
				case 'b':
					// Blink light
					NDeviceHUE_Light_NHUELightList_BlinkLight( lightList,
						i,
						NFALSE );
					break;
				case 'l':
					// Long blink light
					NDeviceHUE_Light_NHUELightList_LongBlinkLight( lightList,
						i,
						NFALSE );
					break;
				case 's':
					// Short blink light
					NDeviceHUE_Light_NHUELightList_ShortBlinkLight( lightList,
						i,
						NFALSE );
					break;
				case 'o':
					// Light on
					NDeviceHUE_Light_NHUELightList_ActivateLight( lightList,
						i,
						NFALSE );
					break;
				case 'f':
					// Light off
					NDeviceHUE_Light_NHUELightList_DeactivateLight( lightList,
						i,
						NFALSE );
					break;
				case 'c':
					// Change color
					NDeviceHUE_Light_NHUELightList_ChangeColor( lightList,
						i,
						NFALSE,
						(NU16)v1,
						(NU8)v2 );
					break;
				case 'i':
					// Change brightness
					NDeviceHUE_Light_NHUELightList_ChangeIntensity( lightList,
						i,
						NFALSE,
						(NU8)v1 );
					break;
				case 'r':
					NDeviceHUE_Light_NHUELightList_ChangeColor4( lightList,
						i,
						NFALSE,
						(NU8)v1,
						(NU8)v2,
						(NU8)v3 );
					break;
				case 'y':
					NDeviceHUE_Light_NHUELightList_ActivateColorLoop( lightList,
						i,
						NFALSE );
					break;
				case 'u':
					NDeviceHUE_Light_NHUELightList_DeactivateColorLoop( lightList,
						i,
						NFALSE );
					break;


				default:
					break;
			}

			// Get light
			if( !( light = (NHUELight*)NDeviceHUE_Light_NHUELightList_GetLightByIndex( lightList,
				i ) ) )
				continue;

			// Is light reachable?
			if( NDeviceHUE_Light_NHUELightState_IsReachable( NDeviceHUE_Light_NHUELight_GetState( light ) ) )
			{
				// Display name
				printf( "%s:\n\n",
					NDeviceHUE_Light_NHUELight_GetName( light ) );

				// Display light details
				NDeviceHUE_Light_NHUELightState_Display( NDeviceHUE_Light_NHUELight_GetState( light ) );

				// New line
				puts( "" );
			}
		}

		// Unlock light list
		NDeviceHUE_Light_NHUELightList_DeactivateProtection( hue->m_light );

		// Ask for an update (will get the current state for the next display)
		NDeviceHUE_NDeviceHUE_UpdateLightList( hue );
	} while( cA != 'q' );

	// Destroy device
	NDeviceHUE_NDeviceHUE_Destroy( &hue );

	// Destroy NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}
#endif // NDEVICE_HUE_TEST_PROJECT

