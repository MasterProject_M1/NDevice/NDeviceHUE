#include "../include/NDeviceHUE.h"

// -----------------------------
// struct NDeviceHUE::NDeviceHUE
// -----------------------------

/**
 * Build a hue device
 *
 * @param name
 * 		The device name
 * @param ip
 * 		The device IP
 * @param maximumAuthenticationRetryCount
 * 		The maximum authentication attempts (0 = infinite)
 * @param autoUpdateDelay
 * 		The auto light state update delay
 *
 * @return the device instance
 */
__ALLOC NDeviceHUE *NDeviceHUE_NDeviceHUE_Build( const char *name,
	const char *ip,
	NU32 maximumAuthenticationRetryCount,
	NU32 autoUpdateDelay )
{
	// Output
	__OUTPUT NDeviceHUE *out;

	// Create database directory
	NLib_Module_Repertoire_CreerRepertoire( NDEVICEHUE_DATABASE_LOCATION );

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NDeviceHUE ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate IP
	if( !( out->m_ip = NLib_Chaine_Dupliquer( ip ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build authentication
	if( !( out->m_authentication = NDeviceHUE_NDeviceHUEAuthentication_Build( name,
		out->m_ip,
		maximumAuthenticationRetryCount ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build light list
	if( !( out->m_light = NDeviceHUE_Light_NHUELightList_Build( out->m_authentication,
		autoUpdateDelay ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy authentication
		NDeviceHUE_NDeviceHUEAuthentication_Destroy( &out->m_authentication );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy a hue device
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_NDeviceHUE_Destroy( NDeviceHUE **this )
{
	// Destroy light list
	NDeviceHUE_Light_NHUELightList_Destroy( &(*this)->m_light );

	// Destroy authentication
	NDeviceHUE_NDeviceHUEAuthentication_Destroy( &(*this)->m_authentication );

	// Free
	NFREE( (*this)->m_ip );
	NFREE( (*this) );
}

/**
 * Get hue device name
 *
 * @param this
 * 		This instance
 *
 * @return the name
 */
const char *NDeviceHUE_NDeviceHUE_GetName( const NDeviceHUE *this )
{
	return NDeviceHUE_NDeviceHUEAuthentication_GetName( this->m_authentication );
}

/**
 * Is ready? (Authenticated?)
 *
 * @param this
 * 		This instance
 *
 * @return if the device is ready (authenticated)
 */
NBOOL NDeviceHUE_NDeviceHUE_IsReady( const NDeviceHUE *this )
{
	return NDeviceHUE_NDeviceHUEAuthentication_IsReady( this->m_authentication );
}

/**
 * Get hue device authentication ID
 *
 * @param this
 * 		This instance
 *
 * @return the id or NULL if not got yet
 */
const char *NDeviceHUE_NDeviceHUE_GetAuthenticationID( const NDeviceHUE *this )
{
	return NDeviceHUE_NDeviceHUEAuthentication_GetAuthenticationID( this->m_authentication );
}

/**
 * Get hue device IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NDeviceHUE_NDeviceHUE_GetIP( const NDeviceHUE *this )
{
	return this->m_ip;
}

/**
 * Get light list
 *
 * @param this
 * 		This instance
 *
 * @return the light list
 */
NHUELightList *NDeviceHUE_NDeviceHUE_GetLightList( const NDeviceHUE *this )
{
	return this->m_light;
}

/**
 * Update light list
 *
 * @param this
 * 		This instance
 *
 * @return if operation started successfully
 */
NBOOL NDeviceHUE_NDeviceHUE_UpdateLightList( NDeviceHUE *this )
{
	return NDeviceHUE_Light_NHUELightList_UpdateLightList( this->m_light );
}

/**
 * Build parser output list describing the current device state
 *
 * @param this
 * 		This instance
 * @param keyRoot
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_NDeviceHUE_BuildParserOutputList( const NDeviceHUE *this,
	const char *keyRoot,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Add authentication informations
	if( !NDeviceHUE_NDeviceHUEAuthentication_BuildParserOutputList( this->m_authentication,
		parserOutputList,
		keyRoot,
		NTRUE )
		|| !NDeviceHUE_Light_NHUELightList_BuildParserOutputList( this->m_light,
			keyRoot,
			parserOutputList,
			NTRUE ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Process GET REST request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in element
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_NDeviceHUE_ProcessRESTGETRequest( const NDeviceHUE *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *element,
	NU32 *cursor )
{
	switch( NDeviceHUE_NDeviceHUEServiceType_FindServiceType( element,
		cursor ) )
	{
		case NDEVICE_HUE_SERVICE_TYPE_ROOT:
			return NDeviceHUE_NDeviceHUE_BuildParserOutputList( this,
				"",
				parserOutputList );

		case NDEVICE_HUE_SERVICE_TYPE_AUTHENTICATION:
			return NDeviceHUE_NDeviceHUEAuthentication_ProcessRESTGETRequest( this->m_authentication,
				parserOutputList,
				element,
				cursor );

		case NDEVICE_HUE_SERVICE_TYPE_LIGHTS:
			return NDeviceHUE_Light_NHUELightList_ProcessRESTGETRequest( this->m_light,
				parserOutputList,
				element,
				cursor );

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Process REST PUT request
 *
 * @param this
 * 		This instance
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param clientDataJson
 * 		The client parsed data
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_NDeviceHUE_ProcessRESTPUTRequest( const NDeviceHUE *this,
	const char *element,
	NU32 *cursor,
	const NParserOutputList *clientDataJson )
{
	// Process
	switch( NDeviceHUE_NDeviceHUEServiceType_FindServiceType( element,
		cursor ) )
	{
		case NDEVICE_HUE_SERVICE_TYPE_ROOT:
		case NDEVICE_HUE_SERVICE_TYPE_LIGHTS:
			// Execute request
			return NDeviceHUE_Light_NHUELightList_ProcessRESTPUTRequest( this->m_light,
				element,
				cursor,
				clientDataJson );

		default:
			return NFALSE;
	}


}

