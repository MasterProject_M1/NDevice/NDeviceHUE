#define NDEVICEHUE_SERVICE_NDEVICEHUELIGHTLISTSERVICETYPE_INTERNE
#include "../../include/NDeviceHUE.h"

// --------------------------------------------------------
// enum NDeviceHUE::Service::NDeviceHUELightListServiceType
// --------------------------------------------------------

/**
 * Get service key
 *
 * @param serviceType
 * 		The service type
 * @param isAddRootKey
 * 		Do we add the root key?
 *
 * @return the service key
 */
const char *NDeviceHUE_Service_NDeviceHUELightListServiceType_GetServiceKey( NDeviceHUELightListServiceType serviceType,
	NBOOL isAddRootKey )
{
	return isAddRootKey ?
		NDeviceHUELightListServiceTypeJson[ serviceType ]
		: NDeviceHUELightListServiceTypeWithoutRoot[ serviceType ];
}

/**
 * Find service type
 *
 * @param element
 * 		The element to get
 * @param cursor
 * 		The cursor into element
 * @param lightList
 * 		The light list (NHUELightList*)
 * @param lightIndexOutput
 * 		The light index found
 *
 * @return the service type
 */
NDeviceHUELightListServiceType NDeviceHUE_Service_NDeviceHUELightListServiceType_FindService( const char *element,
	NU32 *cursor,
	const void *lightList,
	NU32 *lightIndexOutput )
{
	// Iterator
	__OUTPUT NU32 i = 0,
		j;

	// Buffer
	char *buffer;

	// Number
	NU32 number;

	// Light
	const NHUELight *light;

	// Read service
	if( !( buffer = NLib_Chaine_LireJusqua( element,
		'/',
		cursor,
		NFALSE ) ) )
		return NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPES;

	// Look for
	for( ; i < NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPES; i++ )
		switch( i )
		{
			default:
				if( NLib_Chaine_Comparer( buffer,
					NDeviceHUELightListServiceTypeWithoutRoot[ i ],
					NFALSE,
					0 ) )
				{
					// Free
					NFREE( buffer );

					// OK
					return (NDeviceHUELightListServiceType)i;
				}
				break;

			case NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPE_LIGHT_DETAIL:
				// Is it a number?
				if( NLib_Chaine_EstUnNombreEntier( buffer,
					10 ) )
				{
					// Parse number
					number = (NU32)strtol( buffer,
						NULL,
						10 );

					// Check
					if( number < NDeviceHUE_Light_NHUELightList_GetLightCount( lightList ) )
					{
						// Save
						*lightIndexOutput = number;

						// Free
						NFREE( buffer );

						// Found it
						return (NDeviceHUELightListServiceType)i;
					}
				}
				// Is it a name?
				else
					// Iterate lights
					for( j = 0; j < NDeviceHUE_Light_NHUELightList_GetLightCount( lightList ); j++ )
						// Get light
						if( ( light = NDeviceHUE_Light_NHUELightList_GetLightByIndex( lightList,
							j ) ) != NULL )
							// Check name
							if( NLib_Chaine_Comparer( NDeviceHUE_Light_NHUELight_GetName( light ),
								buffer,
								NTRUE,
								0 ) )
							{
								// Save
								*lightIndexOutput = j;

								// Free
								NFREE( buffer );

								// Found it
								return (NDeviceHUELightListServiceType)i;
							}
				break;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NDEVICE_HUE_LIGHT_LIST_SERVICE_TYPES;
}

