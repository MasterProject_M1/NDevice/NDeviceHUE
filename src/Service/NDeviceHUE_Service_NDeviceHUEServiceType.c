#define NDEVICEHUE_SERVICE_NDEVICEHUESERVICETYPE_INTERNE
#include "../../include/NDeviceHUE.h"

// -----------------------------------------------
// enum NDeviceHUE::Service::NDeviceHUEServiceType
// -----------------------------------------------

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceHUE_Service_NDeviceHUEServiceType_GetServiceName( NDeviceHUEServiceType serviceType )
{
	return NDeviceHUEServiceTypeRootURI[ serviceType ];
}

/**
 * Find service type
 *
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type OR NDEVICE_HUE_SERVICE_TYPES
 */
NDeviceHUEServiceType NDeviceHUE_NDeviceHUEServiceType_FindServiceType( const char *element,
	NU32 *cursor )
{
	// Buffer
	char *buffer;

	// Iterator
	__OUTPUT NU32 i;

	// Read element
	if( !( buffer = NLib_Chaine_LireJusqua( element,
		'/',
		cursor,
		NFALSE ) ) )
		return NDEVICE_HUE_SERVICE_TYPES;

	// Look for type
	for( i = 0; i < NDEVICE_HUE_SERVICE_TYPES; i++ )
		if( NLib_Chaine_Comparer( buffer,
			NDeviceHUEServiceTypeRootURI[ i ],
			NFALSE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Found it
			return (NDeviceHUEServiceType)i;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NDEVICE_HUE_SERVICE_TYPES;
}