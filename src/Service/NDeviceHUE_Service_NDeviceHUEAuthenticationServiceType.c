#define NDEVICEHUE_SERVICE_NDEVICEHUEAUTHENTICATIONSERVICETYPE_INTERNE
#include "../../include/NDeviceHUE.h"

// -------------------------------------------------------------
// enum NDeviceHUE::Service::NDeviceHUEAuthenticationServiceType
// -------------------------------------------------------------

/**
 * Get json key for service
 *
 * @param serviceType
 * 		The service type
 * @param isAddAuthenticationRoot
 * 		Do we add the authentication root?
 *
 * @return the json key
 */
const char *NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_GetJsonKey( NDeviceHUEAuthenticationServiceType serviceType,
	NBOOL isAddAuthenticationRoot )
{
	return ( isAddAuthenticationRoot ?
		NDeviceHUEAuthenticationServiceTypeJson[ serviceType ]
		: NDeviceHUEAuthenticationServiceTypeWithoutRoot[ serviceType ] );
}

/**
 * Find service type
 *
 * @param element
 * 		The requested element
 * @param cursor
 * 		The cursor in element
 *
 * @return the service type or NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES
 */
NDeviceHUEAuthenticationServiceType NDeviceHUE_Service_NDeviceHUEAuthenticationServiceType_FindService( const char *element,
	NU32 *cursor )
{
	// Buffer
	char *buffer;

	// Iterator
	__OUTPUT NU32 i = 0;

	// Read
	if( !( buffer = NLib_Chaine_LireJusqua( element,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES;
	}

	// Look for
	for( ; i < NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES; i++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NDeviceHUEAuthenticationServiceTypeWithoutRoot[ i ],
			NFALSE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return (NDeviceHUEAuthenticationServiceType)i;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NDEVICE_HUE_AUTHENTICATION_SERVICE_TYPES;
}

