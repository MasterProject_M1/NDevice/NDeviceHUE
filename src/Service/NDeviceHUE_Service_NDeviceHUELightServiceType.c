#define NDEVICEHUE_SERVICE_NDEVICEHUELIGHTSERVICETYPE_INTERNE
#include "../../include/NDeviceHUE.h"

// ----------------------------------------------------
// enum NDeviceHUE::Service::NDeviceHUELightServiceType
// ----------------------------------------------------

/**
 * Get json key
 *
 * @param serviceType
 * 		The service type
 * @param currentLevel
 * 		The current level
 *
 * @return the json key
 */
const char *NDeviceHUE_Service_NDeviceHUELightServiceType_GetJsonKey( NDeviceHUELightServiceType serviceType,
	const char *currentLevel )
{
	// Cursor
	NU32 cursor = 0;

	// Already in arborescence?
	if( currentLevel != NULL )
		// Move cursor
		if( !NLib_Chaine_PlacerALaChaine( NDeviceHUELightServiceTypeJson[ serviceType ],
			currentLevel,
			&cursor ) )
			// Nothing to return
			return "";

	return NDeviceHUELightServiceTypeJson[ serviceType ] + cursor;
}

/**
 * Get REST key
 *
 * @param serviceType
 * 		The service type
 *
 * @return the rest full key
 */
const char *NDeviceHUE_Service_NDeviceHUELightServiceType_GetRESTKey( NDeviceHUELightServiceType serviceType )
{
	return NDeviceHUELightServiceTypeREST[ serviceType ];
}

/**
 * Find hue mapping for one service
 *
 * @param serviceType
 * 		The service type
 *
 * @return the NHUELightProperty key associated
 */
NU32 NDeviceHUE_Service_NDeviceHUELightServiceType_GetHUEMapping( NDeviceHUELightServiceType serviceType )
{
	switch( serviceType )
	{
		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_NAME:
			return NHUE_LIGHT_PROPERTY_NAME;

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_IS_ON:
			return NHUE_LIGHT_PROPERTY_STATE_IS_ON;

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_BRIGHTNESS:
			return NHUE_LIGHT_PROPERTY_STATE_BRIGHTNESS;

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_SATURATION:
			return NHUE_LIGHT_PROPERTY_STATE_COLOR_SATURATION;

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_COLOR_HUE:
			return NHUE_LIGHT_PROPERTY_STATE_COLOR_HUE;

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_EFFECT:
			return NHUE_LIGHT_PROPERTY_STATE_EFFECT;

		case NDEVICE_HUE_LIGHT_SERVICE_TYPE_STATE_MISC_ALERT:
			return NHUE_LIGHT_PROPERTY_STATE_ALERT;

		default:
			return NHUE_LIGHT_PROPERTIES;
	}
}

/**
 * Find service type
 *
 * @param element
 * 		The element to GET
 * @param separator
 * 		The separator
 * @param cursor
 * 		The cursor into element
 *
 * @return the service type
 */
NDeviceHUELightServiceType NDeviceHUE_Service_NDeviceHUELightServiceType_FindService( const char *element,
	char separator,
	NU32 cursor )
{
	// Buffer
	char *buffer;

	// Element length
	NU32 elementLength;

	// Iterator
	NDeviceHUELightServiceType i = (NDeviceHUELightServiceType)0;

	// Read service
	if( !( buffer = NLib_Chaine_Dupliquer( element + cursor ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NDEVICE_HUE_LIGHT_SERVICE_TYPES;
	}

	// Calculate element length
	elementLength = (NU32)strlen( buffer );

	// Element present?
	if( elementLength > 0 )
		// Remove ending '/'
		if( buffer[ strlen( buffer ) - 1 ] == '/' )
			// Remove
			buffer[ strlen( buffer ) - 1 ] = '\0';

	// Look for service
	for( ; i < NDEVICE_HUE_LIGHT_SERVICE_TYPES; i++ )
		// Check service
		if( NLib_Chaine_Comparer( buffer,
			separator == '/' ?
				NDeviceHUELightServiceTypeREST[ i ]
				: NDeviceHUELightServiceTypeJson[ i ],
			NFALSE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return i;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NDEVICE_HUE_LIGHT_SERVICE_TYPES;
}

/**
 * Get node
 *
 * @param serviceType
 * 		The service type
 *
 * @return the node key
 */
const char *NDeviceHUE_Service_NDeviceHUELightServiceType_GetNodeKey( NDeviceHUELightServiceType serviceType )
{
	return NDeviceHUELightServiceTypeNode[ serviceType ];
}

