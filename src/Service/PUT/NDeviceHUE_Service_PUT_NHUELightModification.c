#include "../../../include/NDeviceHUE.h"

// ------------------------------------------------------
// struct NDeviceHUE::Service::PUT::NHUELightModification
// ------------------------------------------------------

/**
 * Build light modification list
 *
 * @param light
 * 		The light
 *
 * @return the built instance
 */
__ALLOC NHUELightModification *NDeviceHUE_Service_PUT_NHUELightModification_Build( const void *light )
{
	// Output
	__OUTPUT NHUELightModification *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NHUELightModification ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create parser output list
	if( !( out->m_property = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_light = light;

	// OK
	return out;
}

/**
 * Destroy modification list
 *
 * @param this
 * 		This instance
 */
void NDeviceHUE_Service_PUT_NHUELightModification_Destroy( NHUELightModification **this )
{
	// Destroy list
	NParser_Output_NParserOutputList_Destroy( &(*this)->m_property );

	// Free
	NFREE( (*this) );
}

/**
 * Set light
 *
 * @param this
 * 		This instance
 * @param light
 * 		The light
 */
void NDeviceHUE_Service_PUT_NHUELightModification_SetLight( NHUELightModification *this,
	const void *light )
{
	this->m_light = light;
}

/**
 * Add a property with string value
 *
 * @param this
 * 		This instance
 * @param hueLightProperty
 * 		The hue light property property
 * @param value
 * 		The string value
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyString( NHUELightModification *this,
	NU32 hueLightProperty,
	const char *value )
{
	// Add
	return NParser_Output_NParserOutputList_AddEntryString( this->m_property,
		hueLightProperty != NHUE_LIGHT_PROPERTY_NAME ?
			NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( (NHUELightProperty)hueLightProperty )
			: NDeviceHUE_Light_NHUELightProperty_GetProperty( (NHUELightProperty)hueLightProperty ),
		value );
}

/**
 * Add a property with integer value
 *
 * @param this
 * 		This instance
 * @param hueLightProperty
 * 		The hue light property property
 * @param value
 * 		The integer value
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyInteger( NHUELightModification *this,
	NU32 hueLightProperty,
	NU32 value )
{
	// Add
	return NParser_Output_NParserOutputList_AddEntryInteger( this->m_property,
		hueLightProperty != NHUE_LIGHT_PROPERTY_NAME ?
			NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( (NHUELightProperty)hueLightProperty )
			: NDeviceHUE_Light_NHUELightProperty_GetProperty( (NHUELightProperty)hueLightProperty ),
		value );
}

/**
 * Add a property with double value
 *
 * @param this
 * 		This instance
 * @param hueLightProperty
 * 		The hue light property property
 * @param value
 * 		The double value
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyDouble( NHUELightModification *this,
	NU32 hueLightProperty,
	double value )
{
	// Add
	return NParser_Output_NParserOutputList_AddEntryDouble( this->m_property,
		hueLightProperty != NHUE_LIGHT_PROPERTY_NAME ?
			NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( (NHUELightProperty)hueLightProperty )
			: NDeviceHUE_Light_NHUELightProperty_GetProperty( (NHUELightProperty)hueLightProperty ),
		value );
}

/**
 * Add a property with boolean value
 *
 * @param this
 * 		This instance
 * @param hueLightProperty
 * 		The hue light property property
 * @param value
 * 		The boolean value
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyBoolean( NHUELightModification *this,
	NU32 hueLightProperty,
	NBOOL value )
{
	// Add
	return NParser_Output_NParserOutputList_AddEntryBoolean( this->m_property,
		hueLightProperty != NHUE_LIGHT_PROPERTY_NAME ?
			NDeviceHUE_Light_NHUELightProperty_GetShortStateProperty( (NHUELightProperty)hueLightProperty )
			: NDeviceHUE_Light_NHUELightProperty_GetProperty( (NHUELightProperty)hueLightProperty ),
		value );
}

/**
 * Add property
 *
 * @param this
 * 		This instance
 * @param parserOutput
 * 		A parser entry
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_AddProperty( NHUELightModification *this,
	NDeviceHUELightServiceType serviceType,
	const NParserOutput *parserOutput )
{
	// Light property
	NHUELightProperty hueLightProperty;

	// Convert to hue light property
	if( ( hueLightProperty = (NHUELightProperty)NDeviceHUE_Service_NDeviceHUELightServiceType_GetHUEMapping( serviceType ) ) >= NHUE_LIGHT_PROPERTIES )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Add entry
	switch( NParser_Output_NParserOutput_GetType( parserOutput ) )
	{
		case NPARSER_OUTPUT_TYPE_BOOLEAN:
			return NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyBoolean( this,
				hueLightProperty,
				*(NBOOL*)NParser_Output_NParserOutput_GetValue( parserOutput ) );
		case NPARSER_OUTPUT_TYPE_DOUBLE:
			return NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyDouble( this,
				hueLightProperty,
				*(double*)NParser_Output_NParserOutput_GetValue( parserOutput ) );
		case NPARSER_OUTPUT_TYPE_INTEGER:
			return NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyInteger( this,
				hueLightProperty,
				*(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) );
		case NPARSER_OUTPUT_TYPE_STRING:
			return NDeviceHUE_Service_PUT_NHUELightModification_AddPropertyString( this,
				hueLightProperty,
				NParser_Output_NParserOutput_GetValue( parserOutput ) );

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}
}

/**
 * Send modification to light
 *
 * @param this
 * 		This instance
 * @param isStateModification
 * 		Is it a state change or a light property change?
 *
 * @return if operation succeeded
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_SendModification( NHUELightModification *this,
	NBOOL isStateModification )
{
	// Json built data
	char *json;

	// Result
	__OUTPUT NBOOL result;

	// Build json
	if( !( json = NJson_Engine_Builder_Build( this->m_property ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_EXPORT_FAILED );

		// Quit
		return NFALSE;
	}

	// Send
	if( isStateModification )
		result = NDeviceHUE_Light_NHUELight_SendRequest2( this->m_light,
			NTYPE_REQUETE_HTTP_PUT,
			json );
	else
		result = NDeviceHUE_Light_NHUELight_SendRootRequest( this->m_light,
			NTYPE_REQUETE_HTTP_PUT,
			json );

	// Free
	NFREE( json );

	// OK?
	return result;
}

/**
 * Is modification present?
 *
 * @param this
 * 		This instance
 *
 * @return if there are modifications to be applied
 */
NBOOL NDeviceHUE_Service_PUT_NHUELightModification_IsModification( const NHUELightModification *this )
{
	return (NBOOL)( NParser_Output_NParserOutputList_GetEntryCount( this->m_property ) > 0 );
}

