HUE device manager
==================

Introduction
------------

This project is about controlling a philips HUE hub, with the lights
connected to it.

It will allow a final user to build a device, which will try to get an id,
and then save it to a file. This process is automatic, but still need
the user to press the hub button to allow the ID obtention.

Once the id is obtained, no more user action is required, and simple
calls to control function will do what user is asking.

A function called "NDeviceHUE_Light_NHUELightList_UpdateLightList" allows
the user to update light lists in the background in an indepent thread,
to keep simplicity in use.

Functionnalities
----------------

Once id is obtained, the functionnalities are, for one lamp:
ones:

- LightOn( ) : Activate a light
- LightOff( ) : Deactivate a light
- Blink( ) : Blink one time
- LongBlink( ): Blink for 15 seconds (period of about 1 second)
- ActivateColorLoop( ) : Activate a random color loop
- DeactivateColorLoop( ) : Deactivate the color loop
- ChangeColor( Hue, Saturation ) : Change color with hue/saturation properties (HUE: 0 and 65535 = red, 21845 = green, 43690 = blue, saturation: 0 = White, 255 = Colored)
- ChangeColor( X, Y ) : Change color with x/y properties (Look at philips HUE graphics for details)
- ChangeColor( ColorTemperature ) : Change color with temperature property (153 = 6500K to 500 = 2000K)
- ChangeIntensity( Brightness ) : Change light intensity (1-254)

Dependencies
------------

This project depends on

- NLib
- NParser
- NJson
- NHTTP
- NDeviceCommon

Example
-------

An example of use can be found is NDeviceHUE.c

Notes
-----

To reset a bulb registered by another hue hub, execute

```bash
curl 192.168.0.100/api/Fh6jlMczuDYxP3e8-vyaZat9e0HZjr1fHmiPADe3/config/ -X PUT -d "{ \"touchlink\": true }"
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NDevice/NDeviceHUE/

